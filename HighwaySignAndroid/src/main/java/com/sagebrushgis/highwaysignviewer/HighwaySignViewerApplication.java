package com.sagebrushgis.highwaysignviewer;

import android.app.Application;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

import dagger.ObjectGraph;

/**
 * Created with IntelliJ IDEA.
 * User: zachm
 * Date: 4/2/13
 * Time: 5:22 PM
 * To change this template use File | Settings | File Templates.
 */
@ReportsCrashes(
        formKey = "",
        formUri = "http://sagebrushgis.iriscouch.com/acra-sagebrushgis/_design/acra-storage/_update/report",
        reportType = org.acra.sender.HttpSender.Type.JSON,
        httpMethod = org.acra.sender.HttpSender.Method.PUT,
        formUriBasicAuthLogin="highwaysign",
        formUriBasicAuthPassword="highwaysignpwd",
        // Your usual ACRA configuration
        mode = ReportingInteractionMode.TOAST,
        resToastText = R.string.crash_toast_text
)

public class HighwaySignViewerApplication extends Application {
    private ObjectGraph _objectGraph;

    @Override
    public void onCreate(){
        super.onCreate();
        ACRA.init(this);
        _objectGraph = ObjectGraph.create(new HighwaySignViewerApplicationModule(this));

    }

    public void inject(Object object){
        _objectGraph.inject(object);
    }

}
