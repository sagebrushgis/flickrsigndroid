package com.sagebrushgis.highwaysignviewer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sagebrushgis.highwaysignviewer.da.Item;

import java.util.List;

/**
 * Created by zachm on 3/8/14.
 */
public class HeaderArrayAdapter extends ArrayAdapter<Item> {

    private Context _context;
    private List<Item> _items;
    private LayoutInflater _vi;

    public HeaderArrayAdapter(Context context,  List<Item> items) {
        super(context, 0, items);
        _context = context;
        _items = items;
        _vi = (LayoutInflater)_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        final Item i = _items.get(position);
        if (i != null){
            if (i.getHeaderType() == Item.ListHeaderType.SectionHeader){
                v = _vi.inflate(R.layout.item_list_header, null);

                v.setOnClickListener(null);
                v.setOnLongClickListener(null);
                v.setLongClickable(false);

                final TextView sectionView = (TextView) v.findViewById(R.id.list_item_section_text);
                sectionView.setText(i.getText());
            }else if (i.getHeaderType() == Item.ListHeaderType.Default){
                v = _vi.inflate(R.layout.item_list, null);

                final TextView bodyView = (TextView) v.findViewById(R.id.mainText);
                bodyView.setText(i.getText());

                //Some views hide the arrow - make sure the arrow gets hidden
                //By deafult the arrow is visible
                if (!i.showArrow()){
                    ImageView arrow = (ImageView) v.findViewById(R.id.rightArrow);
                    arrow.setVisibility(View.INVISIBLE);
                }
            }
            else {
                v = _vi.inflate(R.layout.item_section_list, null);

                final TextView bodyView = (TextView) v.findViewById(R.id.mainText);
                bodyView.setText(i.getText());
            }
        }


        return v;
    }
}