package com.sagebrushgis.highwaysignviewer.ui.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

/**
 * Created by zachm on 3/2/14.
 */
public class NoNetworkConnectionDialog extends DialogFragment {

    public NoNetworkConnectionDialog(){

    }

    public static NoNetworkConnectionDialog newInstance(){
        NoNetworkConnectionDialog f = new NoNetworkConnectionDialog();
        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle("Highway Sign Viewer");
        alertDialogBuilder.setMessage("No Network Connectivity Detected.  Try Again?");
        alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Save favorite
                dialog.dismiss();
            }
        });

        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        return alertDialogBuilder.create();
    }
}
