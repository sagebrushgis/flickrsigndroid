package com.sagebrushgis.highwaysignviewer.ui.fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.common.collect.Lists;
import com.sagebrushgis.highwaysignviewer.HeaderArrayAdapter;
import com.sagebrushgis.highwaysignviewer.HighwaySignViewerApplication;
import com.sagebrushgis.highwaysignviewer.MainActivity;
import com.sagebrushgis.highwaysignviewer.R;
import com.sagebrushgis.highwaysignviewer.Sign;
import com.sagebrushgis.highwaysignviewer.da.Country;
import com.sagebrushgis.highwaysignviewer.da.HighwayDataSource;
import com.sagebrushgis.highwaysignviewer.da.Item;
import com.sagebrushgis.highwaysignviewer.da.State;
import com.sagebrushgis.highwaysignviewer.events.StateEvent;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.otto.Bus;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by zachm on 1/27/14.
 */
public class StateFragment extends Fragment {
    private HeaderArrayAdapter _adapter;

    MainActivity.BrowseType _browseType;
    private ListView _listView;

    @Inject
    protected Bus bus;

    @Inject
    protected OkHttpClient _client;

    public static StateFragment newInstance(MainActivity.BrowseType browseType) {
        StateFragment f = new StateFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putSerializable("browsetype", browseType);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((HighwaySignViewerApplication)getActivity().getApplication()).inject(this);
        _browseType = (MainActivity.BrowseType) getArguments().getSerializable("browsetype");

        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_country,container, false);

        final List<Item> allItems = new ArrayList<Item>();

        final Context ctx = this.getActivity();

        OnStateResults results = new OnStateResults() {
            @Override
            public void statesLoaded(List<Country> countries) {
                for (Country c : countries){
                    allItems.add(c);
                    for (State s : c.getStates()){
                        allItems.add(s);
                    }
                }

                _adapter = new HeaderArrayAdapter(ctx,allItems);
                _listView.setAdapter(_adapter);
            }
        };

        StateRequest req = new StateRequest(results);
        req.execute();


        _listView = (ListView)view.findViewById(R.id.country_list);
        _listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                State state = (State)_listView.getItemAtPosition (position);
                bus.post(new StateEvent(state,_browseType));
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        bus.register(this);
    }

    @Override
    public void onPause() {
        super.onDestroy();
        bus.unregister(this);
    }

    private class StateRequest extends AsyncTask<Void,Void,String> {

        private OnStateResults _stateResults;

        public StateRequest(OnStateResults stateResults){
            _stateResults = stateResults;
        }

        private final String _baseFavoriteUrl = getString(R.string.state_json);


        @Override
        public String doInBackground(Void...params) {
            //First parameter is the type enum


            URL url = null;
            try {
                url = new URL(_baseFavoriteUrl);

                URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
                url = uri.toURL();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            try {
                HttpURLConnection conn = _client.open(url);

                InputStream inputStream = conn.getInputStream();

                Reader in = new InputStreamReader(inputStream);
                BufferedReader buffRead = new BufferedReader(in);
                StringBuilder stringBuilder = new StringBuilder();

                String curLine = null;

                while ((curLine = buffRead.readLine()) != null) {
                    stringBuilder.append(curLine);
                }


                return stringBuilder.toString();
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            List<Country> countryList = new ArrayList<Country>();
            if (s == null || s.length() == 0 )
                _stateResults.statesLoaded(countryList);




            JSONArray countryArray = null;
            try {
                countryArray = new JSONArray(s);

                for (int j = 0; j <= countryArray.length() - 1; j++) {
                    JSONObject countryJson = countryArray.getJSONObject(j);
                    Country c = new Country();
                    c.setName(countryJson.getString("country"));
                    c.setSlug(countryJson.getString("slug"));

                    List<State> states = new ArrayList<State>();

                    JSONArray stateArray = countryJson.getJSONArray("states");

                    for (int i = 0; i <= stateArray.length() - 1; i++) {

                        JSONObject stateJson = stateArray.getJSONObject(i);
                        State state = new State();
                        state.setName(stateJson.getString("state"));
                        state.setSlug(stateJson.getString("slug"));


                        states.add(state);
                    }

                    c.setStates(states);
                    countryList.add(c);
                }


                _stateResults.statesLoaded(countryList);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public interface OnStateResults{
        void statesLoaded(List<Country> countries);
    }

}
