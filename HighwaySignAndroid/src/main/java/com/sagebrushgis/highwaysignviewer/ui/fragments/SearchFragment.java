package com.sagebrushgis.highwaysignviewer.ui.fragments;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.support.v4.app.Fragment;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;


import com.sagebrushgis.highwaysignviewer.HighwaySignViewerApplication;
import com.sagebrushgis.highwaysignviewer.PlacesAutoCompleteAdapter;
import com.sagebrushgis.highwaysignviewer.R;
import com.sagebrushgis.highwaysignviewer.events.LocationEvent;
import com.squareup.otto.Bus;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import javax.inject.Inject;

/**
 * Created by zachm on 2/1/14.
 */
public class SearchFragment extends Fragment implements AdapterView.OnItemClickListener {
    @Inject
    protected Bus bus;

    private TextView searchRadiusTextView;
    private Button searchButton;
    private AutoCompleteTextView autoComplete;

    private double _latitude;
    private double _longitude;

    private String _googleMapsApiKey;

    public static SearchFragment newInstance(){
        SearchFragment f = new SearchFragment();
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        _googleMapsApiKey = getGoogleMapsApiKey();
        ((HighwaySignViewerApplication)getActivity().getApplication()).inject(this);
        setRetainInstance(true);
    }

    private String getGoogleMapsApiKey(){
        try{
            ApplicationInfo ai = getActivity().getPackageManager().getApplicationInfo(getActivity().getPackageName(), PackageManager.GET_META_DATA);
            Bundle bundle = ai.metaData;
            return bundle.getString("com.sagebrushgis.highwaysignviewer.GoogleWebKey");
        }catch (Exception e){
            return "";
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        bus.register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        bus.unregister(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_search,container, false);

        searchRadiusTextView = (TextView)view.findViewById(R.id.searchRadius);
        searchButton = (Button)view.findViewById(R.id.searchButton);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getActivity().getCurrentFocus() != null) {
                    InputMethodManager inputMethodManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                }

                bus.post(new LocationEvent(_latitude,_longitude));
            }
        });
        searchButton.setEnabled(false);
        autoComplete = (AutoCompleteTextView)view.findViewById(R.id.autoCompleteTextView1);

        SharedPreferences settings = getActivity().getPreferences(0);
        int searchRadius = settings.getInt("searchRadius", 10);
        searchRadiusTextView.setText(String.valueOf(searchRadius));

        autoComplete.setEnabled(true);
        autoComplete.setAdapter(new PlacesAutoCompleteAdapter(getActivity(), R.layout.item_list, _googleMapsApiKey));
        autoComplete.setOnItemClickListener(this);
        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String item = (String) parent.getItemAtPosition(position);
        new GeocodeAsync().execute(item);
    }
    private class GeocodeAsync extends AsyncTask<String,Void,String> {

        @Override
        public String doInBackground(String... address){
            try {
                URL url = new URL("http://maps.googleapis.com/maps/api/geocode/json?address=" +
                        URLEncoder.encode(address[0], "utf-8") +
                        "&sensor=true");

                URLConnection tc = url.openConnection();
                BufferedReader in = new BufferedReader(new InputStreamReader(tc.getInputStream()));

                String line;
                StringBuffer sb = new StringBuffer();
                //take Google's legible JSON and turn it into one big string.
                while ((line = in.readLine()) != null) {
                    sb.append(line);
                }

                return sb.toString();
            } catch (UnsupportedEncodingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return "";
        }

        @Override
        protected void onPostExecute(String result){
            try{
                //turn that string into a JSON object
                JSONObject geocodeResults = new JSONObject(result);
                //now get the JSON array that's inside that object

                JSONArray arrayOfAddressResults = geocodeResults.getJSONArray("results");
                JSONObject addressItem = arrayOfAddressResults.getJSONObject(0);
                JSONObject jsonGeometry = addressItem.getJSONObject("geometry");
                JSONObject jsonLocation = jsonGeometry.getJSONObject("location");
                _latitude  = jsonLocation.getDouble("lat");
                _longitude = jsonLocation.getDouble("lng");
                searchButton.setEnabled(true);
            }    catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
    }

}
