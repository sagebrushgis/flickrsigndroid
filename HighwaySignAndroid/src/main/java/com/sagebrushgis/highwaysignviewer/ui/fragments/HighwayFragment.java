package com.sagebrushgis.highwaysignviewer.ui.fragments;

import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;


import com.sagebrushgis.highwaysignviewer.HighwaySignViewerApplication;
import com.sagebrushgis.highwaysignviewer.R;
import com.sagebrushgis.highwaysignviewer.da.County;
import com.sagebrushgis.highwaysignviewer.da.Highway;
import com.sagebrushgis.highwaysignviewer.da.HighwayDataSource;
import com.sagebrushgis.highwaysignviewer.da.HighwayType;
import com.sagebrushgis.highwaysignviewer.da.State;
import com.sagebrushgis.highwaysignviewer.events.HighwayStateEvent;
import com.sagebrushgis.highwaysignviewer.events.HighwayTypeHighwayEvent;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.otto.Bus;
import com.squareup.picasso.Picasso;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by zachm on 1/28/14.
 */
public class HighwayFragment extends Fragment {

    @Inject
    protected Bus bus;

    @Inject
    protected Picasso picasso;

    @Inject
    protected OkHttpClient _client;

    private State _state;
    private HighwayType _hwyType;
    private ArrayAdapter<Highway> _adapter;
    private ListView _listView;

    private boolean _getHighwaysByState;
    public static HighwayFragment newInstance(State state){

        HighwayFragment f = new HighwayFragment();

        // Supply state input as an argument.
        Bundle args = new Bundle();
        args.putParcelable("state", state);
        f.setArguments(args);

        return f;
    }

    public static HighwayFragment newInstance(HighwayType highwayType){

        HighwayFragment f = new HighwayFragment();

        // Supply state input as an argument.
        Bundle args = new Bundle();
        args.putParcelable("highwaytype", highwayType);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((HighwaySignViewerApplication)getActivity().getApplication()).inject(this);
        setRetainInstance(true);


        if (getArguments().containsKey("state")){
            _state = getArguments().getParcelable("state");
            _getHighwaysByState = true;
        }
        else{
            _hwyType = getArguments().getParcelable("highwaytype");
            _getHighwaysByState = false;
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_country,container, false);
        _listView = (ListView)view.findViewById(R.id.country_list);


        OnHighwayResults res = new OnHighwayResults() {
            @Override
            public void highwaysLoaded(List<Highway> highways) {
                _adapter = new HighwayArrayAdapter(getActivity(),R.layout.hwy_list_row,R.id.highway,highways);

                _listView.setAdapter(_adapter);
            }
        };

        if (_getHighwaysByState){
            StateRequest req = new StateRequest(res);
            req.execute(_state);
        }
        else
        {
            HighwayTypeRequest req = new HighwayTypeRequest(res);
            req.execute(_hwyType);
        }



        _listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    Highway highway = (Highway)_listView.getItemAtPosition(position);
                if (_getHighwaysByState){
                    bus.post(new HighwayStateEvent(highway,_state));
                }else{
                    bus.post(new HighwayTypeHighwayEvent(_hwyType,highway));
                }
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        bus.register(this);
    }

    @Override
    public void onPause() {
        super.onDestroy();
        bus.unregister(this);
    }


    private class StateRequest extends AsyncTask<State,Void,String> {

        private OnHighwayResults _highwayResults;

        public StateRequest(OnHighwayResults highwayResults){
            _highwayResults = highwayResults;
        }

        private final String _baseUrl = "http://www.sagebrushgis.com/highwaystate/?state=";


        @Override
        public String doInBackground(State...params) {
            //First parameter is the type enum

            State s = params[0];

            URL url = null;
            try {
                url = new URL(_baseUrl + s.getSlug());

                URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
                url = uri.toURL();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            try {
                HttpURLConnection conn = _client.open(url);

                InputStream inputStream = conn.getInputStream();

                Reader in = new InputStreamReader(inputStream);
                BufferedReader buffRead = new BufferedReader(in);
                StringBuilder stringBuilder = new StringBuilder();

                String curLine = null;

                while ((curLine = buffRead.readLine()) != null) {
                    stringBuilder.append(curLine);
                }


                return stringBuilder.toString();
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            List<Highway> highwayList = new ArrayList<Highway>();
            if (s == null || s.length() == 0 )
                _highwayResults.highwaysLoaded(highwayList);




            JSONArray countyArray = null;
            try {
                countyArray = new JSONArray(s);

                for (int j = 0; j <= countyArray.length() - 1; j++) {
                    JSONObject countyJson = countyArray.getJSONObject(j);
                    highwayList.add(Highway.fromJson(countyJson));
                }

                _highwayResults.highwaysLoaded(highwayList);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private class HighwayTypeRequest extends AsyncTask<HighwayType,Void,String> {

        private OnHighwayResults _highwayResults;

        public HighwayTypeRequest(OnHighwayResults highwayResults){
            _highwayResults = highwayResults;
        }

        private final String _baseUrl = "http://www.sagebrushgis.com/highwaybytype/?highwaytype=";


        @Override
        public String doInBackground(HighwayType...params) {
            //First parameter is the type enum

            HighwayType h = params[0];

            URL url = null;
            try {
                url = new URL(_baseUrl + h.getSlug());

                URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
                url = uri.toURL();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            try {
                HttpURLConnection conn = _client.open(url);

                InputStream inputStream = conn.getInputStream();

                Reader in = new InputStreamReader(inputStream);
                BufferedReader buffRead = new BufferedReader(in);
                StringBuilder stringBuilder = new StringBuilder();

                String curLine = null;

                while ((curLine = buffRead.readLine()) != null) {
                    stringBuilder.append(curLine);
                }


                return stringBuilder.toString();
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            List<Highway> highwayList = new ArrayList<Highway>();
            if (s == null || s.length() == 0 )
                _highwayResults.highwaysLoaded(highwayList);




            JSONArray countyArray = null;
            try {
                countyArray = new JSONArray(s);

                for (int j = 0; j <= countyArray.length() - 1; j++) {
                    JSONObject countyJson = countyArray.getJSONObject(j);
                    highwayList.add(Highway.fromJson(countyJson));
                }

                _highwayResults.highwaysLoaded(highwayList);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    interface OnHighwayResults{
        void highwaysLoaded(List<Highway> highways);
    }

    private class HighwayArrayAdapter extends ArrayAdapter<Highway> {
        private LayoutInflater inflater = null;
        private List<Highway> _highways;

        public HighwayArrayAdapter (Context context, int resource,
                                    int textViewResourceId, List<Highway> objects) {
            super(context, resource, textViewResourceId, objects);

            inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            _highways = objects;

        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            if (convertView == null)
                vi = inflater.inflate(R.layout.hwy_list_row,null);

            TextView title = (TextView)vi.findViewById(R.id.highway);
            ImageView thumbImage = (ImageView)vi.findViewById(R.id.sign_image);

            Highway hwy = _highways.get(position);

            //Set values in list view
            title.setText(hwy.getHighway());
            String url = getString(R.string.small_shield_url) + hwy.getImageName();
            picasso.with(getActivity()).load(url).into(thumbImage);
            return vi;
        }

    }
}
