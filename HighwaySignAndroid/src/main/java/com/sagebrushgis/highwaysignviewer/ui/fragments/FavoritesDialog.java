package com.sagebrushgis.highwaysignviewer.ui.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import com.sagebrushgis.highwaysignviewer.da.HighwayDataSource;

/**
 * Created by zachm on 2/18/14.
 */
public class FavoritesDialog extends DialogFragment {

    private HighwayDataSource _dataSource;

    public FavoritesDialog() {

    }

    public static FavoritesDialog newInstance(long imageId){
        FavoritesDialog f = new FavoritesDialog();

        // Supply image id as an argument
        Bundle args = new Bundle();
        args.putLong("imageid", imageId);
        f.setArguments(args);

        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final long imageId = getArguments().getLong("imageid");
        _dataSource = new HighwayDataSource(getActivity());
        _dataSource.open();

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle("Highway Sign Viewer");
        alertDialogBuilder.setMessage("Add Sign To Favorites?");
        alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Save favorite
                _dataSource.insertFavorite(imageId);
                dialog.dismiss();
            }
        });

        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        return alertDialogBuilder.create();
    }
}
