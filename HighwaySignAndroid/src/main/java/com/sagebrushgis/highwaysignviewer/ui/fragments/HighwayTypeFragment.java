package com.sagebrushgis.highwaysignviewer.ui.fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.sagebrushgis.highwaysignviewer.HeaderArrayAdapter;
import com.sagebrushgis.highwaysignviewer.HighwaySignViewerApplication;
import com.sagebrushgis.highwaysignviewer.R;
import com.sagebrushgis.highwaysignviewer.da.Country;
import com.sagebrushgis.highwaysignviewer.da.HighwayDataSource;
import com.sagebrushgis.highwaysignviewer.da.HighwayType;
import com.sagebrushgis.highwaysignviewer.da.Item;
import com.sagebrushgis.highwaysignviewer.da.State;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.otto.Bus;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;


/**
 * Created by zachm on 2/2/14.
 */
public class HighwayTypeFragment extends Fragment {


    private HeaderArrayAdapter _adapter;
    private ListView _listView;

    @Inject
    protected Bus bus;

    @Inject
    protected OkHttpClient _client;

    public static HighwayTypeFragment newInstance(){
        HighwayTypeFragment f = new HighwayTypeFragment();

        Bundle bundle = new Bundle();
        f.setArguments(bundle);

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        ((HighwaySignViewerApplication)getActivity().getApplication()).inject(this);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_country,container, false);

        final List<Item> allItems = new ArrayList<Item>();
        final Context ctx = getActivity();
        OnHighwayTypeResults res = new OnHighwayTypeResults() {
            @Override
            public void highwayTypesLoaded(List<Country> hwyTypes) {
                for (Country c : hwyTypes){
                    allItems.add(c);
                    for (HighwayType t : c.getHighwayTypes()){
                            allItems.add(t);
                    }
                }

                _adapter = new HeaderArrayAdapter(ctx,allItems);
                _listView.setAdapter(_adapter);
            }
        };

        HighwayTypeRequest req = new HighwayTypeRequest(res);
        req.execute();

        _listView = (ListView)view.findViewById(R.id.country_list);
        _listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                HighwayType hwyType = (HighwayType)_listView.getItemAtPosition (position);
                bus.post(hwyType);
            }
        });

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        bus.unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        bus.register(this);
    }

    private class HighwayTypeRequest extends AsyncTask<Void,Void,String> {

        private OnHighwayTypeResults _hwyTypeResults;

        public HighwayTypeRequest(OnHighwayTypeResults hwyTypeResults){
            _hwyTypeResults = hwyTypeResults;
        }

        private final String _baseFavoriteUrl = getString(R.string.highwaytype_json);


        @Override
        public String doInBackground(Void...params) {
            //First parameter is the type enum


            URL url = null;
            try {
                url = new URL(_baseFavoriteUrl);

                URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
                url = uri.toURL();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            try {
                HttpURLConnection conn = _client.open(url);

                InputStream inputStream = conn.getInputStream();

                Reader in = new InputStreamReader(inputStream);
                BufferedReader buffRead = new BufferedReader(in);
                StringBuilder stringBuilder = new StringBuilder();

                String curLine = null;

                while ((curLine = buffRead.readLine()) != null) {
                    stringBuilder.append(curLine);
                }


                return stringBuilder.toString();
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            List<Country> countryList = new ArrayList<Country>();
            if (s == null || s.length() == 0 )
                _hwyTypeResults.highwayTypesLoaded(countryList);




            JSONArray countryArray = null;
            try {
                countryArray = new JSONArray(s);

                for (int j = 0; j <= countryArray.length() - 1; j++) {
                    JSONObject countryJson = countryArray.getJSONObject(j);
                    Country c = new Country();
                    c.setName(countryJson.getString("name"));
                    c.setSlug(countryJson.getString("slug"));

                    List<HighwayType> highwayTypes = new ArrayList<HighwayType>();

                    JSONArray highwayTypeArray = countryJson.getJSONArray("types");

                    for (int i = 0; i <= highwayTypeArray.length() - 1; i++) {

                        JSONObject stateJson = highwayTypeArray.getJSONObject(i);
                        HighwayType hwyType = new HighwayType();
                        hwyType.setType(stateJson.getString("type"));
                        hwyType.setSlug(stateJson.getString("slug"));
                        hwyType.setSort(stateJson.getInt("sort"));


                        highwayTypes.add(hwyType);
                    }

                    Collections.sort(highwayTypes);
                    c.setHighwayTypes(highwayTypes);
                    countryList.add(c);
                }


                _hwyTypeResults.highwayTypesLoaded(countryList);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public interface OnHighwayTypeResults{
        void highwayTypesLoaded(List<Country> hwyTypes);
    }
}
