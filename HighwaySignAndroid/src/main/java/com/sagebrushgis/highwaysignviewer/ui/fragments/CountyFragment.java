package com.sagebrushgis.highwaysignviewer.ui.fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.sagebrushgis.highwaysignviewer.HighwaySignViewerApplication;
import com.sagebrushgis.highwaysignviewer.R;
import com.sagebrushgis.highwaysignviewer.da.Country;
import com.sagebrushgis.highwaysignviewer.da.County;
import com.sagebrushgis.highwaysignviewer.da.Highway;
import com.sagebrushgis.highwaysignviewer.da.HighwayDataSource;
import com.sagebrushgis.highwaysignviewer.da.Item;
import com.sagebrushgis.highwaysignviewer.da.State;
import com.sagebrushgis.highwaysignviewer.events.StateCountyEvent;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.otto.Bus;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by zachm on 2/16/14.
 */
public class CountyFragment extends Fragment {

    private State _state;
    private List<County> _counties;
    private ArrayAdapter<County> _adapter;

    @Inject
    protected OkHttpClient _client;

    @Inject
    protected Bus bus;

    public static CountyFragment newInstance(State state){
        CountyFragment f = new CountyFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putParcelable("state", state);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((HighwaySignViewerApplication)getActivity().getApplication()).inject(this);
        _state = getArguments().getParcelable("state");

        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_country,container, false);


        _adapter = new ArrayAdapter<County>(this.getActivity(),R.layout.text_row,R.id.listmainitem);

        final ListView listView = (ListView)view.findViewById(R.id.country_list);

        OnCountyResults res = new OnCountyResults() {
            @Override
            public void countiesLoaded(List<County> counties) {
                for (County c : counties)
                    _adapter.add(c);

                listView.setAdapter(_adapter);
            }
        };

        CountyRequest req = new CountyRequest(res);
        req.execute(_state);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                County county = (County)listView.getItemAtPosition (position);
                bus.post(new StateCountyEvent(county,_state));
            }
        });


        return view;
    }

    private class CountyRequest extends AsyncTask<State,Void,String> {

        private OnCountyResults _countyResults;

        public CountyRequest(OnCountyResults countyResults){
            _countyResults = countyResults;
        }

        private final String _baseFavoriteUrl = getString(R.string.county_json);


        @Override
        public String doInBackground(State...params) {
            //First parameter is the type enum

            State s = params[0];

            URL url = null;
            try {
                url = new URL(_baseFavoriteUrl + s.getSlug());

                URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
                url = uri.toURL();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            try {
                HttpURLConnection conn = _client.open(url);

                InputStream inputStream = conn.getInputStream();

                Reader in = new InputStreamReader(inputStream);
                BufferedReader buffRead = new BufferedReader(in);
                StringBuilder stringBuilder = new StringBuilder();

                String curLine = null;

                while ((curLine = buffRead.readLine()) != null) {
                    stringBuilder.append(curLine);
                }


                return stringBuilder.toString();
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            List<County> countyList = new ArrayList<County>();
            if (s == null || s.length() == 0 )
                _countyResults.countiesLoaded(countyList);




            JSONArray countyArray = null;
            try {
                countyArray = new JSONArray(s);

                for (int j = 0; j <= countyArray.length() - 1; j++) {
                    JSONObject countyJson = countyArray.getJSONObject(j);
                    County c = new County();
                    c.setName(countyJson.getString("name"));
                    c.setSlug(countyJson.getString("slug"));
                    c.setType(countyJson.getString("type"));

                    countyList.add(c);
                }

                _countyResults.countiesLoaded(countyList);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    interface OnCountyResults{
        void countiesLoaded(List<County> counties);
    }
}
