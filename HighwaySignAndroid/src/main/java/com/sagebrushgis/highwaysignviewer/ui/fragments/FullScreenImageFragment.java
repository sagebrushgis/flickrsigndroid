package com.sagebrushgis.highwaysignviewer.ui.fragments;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.sagebrushgis.highwaysignviewer.HighwaySignViewerApplication;
import com.sagebrushgis.highwaysignviewer.R;
import com.sagebrushgis.highwaysignviewer.Sign;
import com.sagebrushgis.highwaysignviewer.TouchImageView;
import com.sagebrushgis.highwaysignviewer.da.HighwayDataSource;
import com.sagebrushgis.highwaysignviewer.events.AddFavoritesEvent;
import com.sagebrushgis.highwaysignviewer.events.FullScreenImageVisible;
import com.squareup.otto.Bus;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

/**
 * Created with IntelliJ IDEA.
 * User: zachm
 * Date: 4/17/13
 * Time: 12:41 PM
 * To change this template use File | Settings | File Templates.
 */
public class FullScreenImageFragment extends Fragment {

    @Inject
    protected Bus bus;

    @Inject
    protected Picasso picasso;

    TouchImageView imageView;

    private Sign s;


    public static FullScreenImageFragment newInstance(Sign sign) {
        FullScreenImageFragment f = new FullScreenImageFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putParcelable("sign", sign);
        f.setArguments(args);

        return f;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        s = getArguments().getParcelable("sign");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_viewsign,container, false);

        ((HighwaySignViewerApplication)getActivity().getApplication()).inject(this);
        imageView = (TouchImageView)view.findViewById(R.id.imgDisplay);

        picasso.with(getActivity()).load(s.getLargeImageURL()).into(imageView);

        Toast toast = Toast.makeText(getActivity(), "Pinch Image To Zoom In and Out", 100);
        toast.show();

        bus.post(new FullScreenImageVisible(s));
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.imagedetails_menu, menu);
        super.onCreateOptionsMenu(menu,inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_favorites:
                bus.post(new AddFavoritesEvent(s.getFlickrID()));
            case R.id.clear_favorites:
                HighwayDataSource ds = new HighwayDataSource(getActivity());
                ds.open();
                ds.clearFavorites();
            default:
                break;
        }

        return true;
    }
}
