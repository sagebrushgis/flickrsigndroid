package com.sagebrushgis.highwaysignviewer.ui.fragments;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.sagebrushgis.highwaysignviewer.Coordinates;
import com.sagebrushgis.highwaysignviewer.EndlessScrollListener;
import com.sagebrushgis.highwaysignviewer.HighwaySignViewerApplication;
import com.sagebrushgis.highwaysignviewer.ImageQueries;
import com.sagebrushgis.highwaysignviewer.R;
import com.sagebrushgis.highwaysignviewer.Sign;
import com.sagebrushgis.highwaysignviewer.SignAdapter;
import com.sagebrushgis.highwaysignviewer.da.County;
import com.sagebrushgis.highwaysignviewer.da.Favorite;
import com.sagebrushgis.highwaysignviewer.da.Highway;
import com.sagebrushgis.highwaysignviewer.da.State;
import com.sagebrushgis.highwaysignviewer.events.NoSignsEvent;
import com.sagebrushgis.highwaysignviewer.events.RenameSignEvent;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.otto.Bus;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by zachm on 1/28/14.
 */
public class ResultsFragment extends Fragment implements ImageQueries.ImageQueryResult{
    @Inject
    protected Bus bus;

    @Inject
    protected OkHttpClient _client;

    enum QueryType {GEO, HIGHWAY, STATEHIGHWAY, STATECOUNTY, FAVORITE};

    private QueryType _curType;
    private State _state;
    private County _county;
    private String _highway;
    private String _latitude;
    private String _longitude;
    private String _searchRadius;
    private SignAdapter _adapter;
    private Coordinates _coordinates;
    private List<Favorite> _favorites;

    public boolean _morePages = true;

    public static ResultsFragment newInstance(State state,County county){
        ResultsFragment f = new ResultsFragment();

        // Add state and county to bundle
        Bundle args = new Bundle();
        args.putParcelable("state", state);
        args.putParcelable("county", county);
        f.setArguments(args);

        return f;
    }

    public static ResultsFragment newInstance(double latitude, double longitude, int radius){
        ResultsFragment f = new ResultsFragment();

        // Add state and highway to bundle
        Bundle args = new Bundle();
        args.putDouble("latitude", latitude);
        args.putDouble("longitude", longitude);
        args.putInt("radius", radius);
        f.setArguments(args);

        return f;
    } 
    public static ResultsFragment newInstance(State state, Highway highway){
        ResultsFragment f = new ResultsFragment();

        // Add state and highway to bundle
        Bundle args = new Bundle();
        args.putParcelable("state", state);
        args.putString("highway", highway.getTagID());
        f.setArguments(args);

        return f;
    }

    public static ResultsFragment newInstance(Highway highway){
        ResultsFragment f = new ResultsFragment();

        // Add state and highway to bundle
        Bundle args = new Bundle();

        args.putString("highway", highway.getTagID());
        f.setArguments(args);

        return f;
    }

    public static ResultsFragment newInstance(List<Favorite> favorites){
        ResultsFragment f = new ResultsFragment();

        //Add favorites to bundle
        Bundle args = new Bundle();

        args.putParcelableArrayList("favorites", (ArrayList<Favorite>) favorites);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((HighwaySignViewerApplication)getActivity().getApplication()).inject(this);

        if (getArguments().containsKey("latitude")){
            _latitude = String.valueOf(getArguments().getDouble("latitude"));
            _longitude = String.valueOf(getArguments().getDouble("longitude"));
            _searchRadius = String.valueOf(getArguments().getInt("radius"));
            _coordinates = new Coordinates(getArguments().getDouble("latitude"),getArguments().getDouble("longitude"));
            _curType = QueryType.GEO;

            if (_morePages){
                ImageQueries queries = new ImageQueries(this,_client);
                queries.getImageByLocation(Double.parseDouble(_latitude),Double.parseDouble(_longitude), Integer.parseInt(_searchRadius));
            }
        }else if (getArguments().containsKey("county")){
            _state = getArguments().getParcelable("state");
            _county = getArguments().getParcelable("county");

            _curType = QueryType.STATECOUNTY;
            if (_morePages){
                ImageQueries queries = new ImageQueries(this,_client);
                queries.getImageByStateCounty(_state, _county);
            }
        }else if (getArguments().containsKey("state")){
            _state = getArguments().getParcelable("state");
            _highway = getArguments().getString("highway");

            _curType = QueryType.STATEHIGHWAY;
            if (_morePages){
                ImageQueries queries = new ImageQueries(this,_client);
                queries.getImageByStateHighway(_state,_highway);
            }
        }else if (getArguments().containsKey("favorites")){
            _favorites = getArguments().getParcelableArrayList("favorites");

            _curType = QueryType.FAVORITE;
            if (_morePages){
                ImageQueries queries = new ImageQueries(this,_client);
                queries.getFavoriteImages(_favorites);
            }
        }
        else{
            _highway = getArguments().getString("highway");

            _curType = QueryType.HIGHWAY;
            if (_morePages){
                ImageQueries queries = new ImageQueries(this,_client);
                queries.getImageByHighway(_highway);
            }
        }

        if (_curType == QueryType.GEO){
            Coordinates coords = new Coordinates(getArguments().getDouble("latitude"),getArguments().getDouble("longitude"));
            _adapter = new SignAdapter(getActivity(),coords);
        }
        else
            _adapter = new SignAdapter(getActivity());
        setRetainInstance(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        bus.register(this);
    }

    @Override
    public void onPause() {
        super.onDestroy();
        bus.unregister(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_results,container, false);
        ListView gridView = (ListView)view.findViewById(R.id.gridview);
        gridView.setAdapter(_adapter);
        gridView.setOnScrollListener(new EndlessScrollListener() {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                fetchNextPage(page);
            }
        });

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Sign sign = (Sign)parent.getItemAtPosition (position);

                bus.post(new RenameSignEvent(sign,false));
            }
        });

        return view;
    }

    public void fetchNextPage(int page){
        if (_morePages){
            ImageQueries queries = new ImageQueries(this,_client);
            if (_curType == QueryType.GEO){
                queries.getImageByLocation(Double.parseDouble(_latitude), Double.parseDouble(_longitude), Integer.parseInt(_searchRadius), page);
            }
            else if (_curType == QueryType.STATEHIGHWAY){
                queries.getImageByStateHighway(_state, _highway, page);
            }
            else if(_curType == QueryType.STATECOUNTY){
                queries.getImageByStateCounty(_state,_county, page);
            }
            else{
                queries.getImageByHighway(_highway, page);
            }
        }
    }

    @Override
    public void OnImageQueryComplete(ImageQueries.QueryType queryType, List<Sign> signs, int page, int pages, boolean initial) {
        _morePages = page < pages;

        if (signs.isEmpty()){
            bus.post(new NoSignsEvent());
        } 
        if (queryType == ImageQueries.QueryType.RANDOM){
            /*if (signs.size() > 0){
                Intent intent = new Intent(getBaseContext(), ViewSignActivity.class);
                Bundle b = new Bundle();
                b.putParcelable("sign", signs.get(0));

                intent.putExtras(b);
                startActivity(intent);
            }*/
        }else{
            _adapter.updateSigns(signs);
        }
    }

}
