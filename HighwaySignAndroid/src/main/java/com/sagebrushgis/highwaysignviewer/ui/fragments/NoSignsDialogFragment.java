package com.sagebrushgis.highwaysignviewer.ui.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

/**
 * Created by zachm on 2/17/14.
 */
public class NoSignsDialogFragment extends DialogFragment {
    public NoSignsDialogFragment(){

    }

    public static NoSignsDialogFragment newInstance(){
        NoSignsDialogFragment f = new NoSignsDialogFragment();
        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle("Highway Sign Viewer");
        alertDialogBuilder.setMessage("No Signs Found At Location.  Generating Random Sign Instead.");
        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Get Random Sign but now just dismiss
                dialog.dismiss();
                ((DialogFragmentListener)getActivity()).onNoSignDialogFragmentClosing();
            }
        });

        return alertDialogBuilder.create();

    }

    public interface DialogFragmentListener{
        void onNoSignDialogFragmentClosing();
    }
}
