package com.sagebrushgis.highwaysignviewer.ui.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.GridLayout;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sagebrushgis.highwaysignviewer.Sign;

/**
 * Created by zachm on 1/15/14.
 */
public class TransparentSupportMapFragment extends SupportMapFragment {

    private Sign _sign;

    public TransparentSupportMapFragment(){
        super();
    }

    public static TransparentSupportMapFragment newInstance (Sign sign){
        TransparentSupportMapFragment f = new TransparentSupportMapFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putParcelable("sign", sign);
        f.setArguments(args);

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup view, Bundle savedInstance) {

        View layout = super.onCreateView(inflater, view, savedInstance);
        _sign = getArguments().getParcelable("sign");

        FrameLayout frameLayout = new FrameLayout(getActivity());
        frameLayout.setBackgroundColor(getResources().getColor(android.R.color.transparent));
        ((ViewGroup) layout).addView(frameLayout, new ViewGroup.LayoutParams(GridLayout.LayoutParams.MATCH_PARENT, GridLayout.LayoutParams.MATCH_PARENT));

        return layout;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        GoogleMap map = getMap();
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        map.setMyLocationEnabled(true);
        LatLng latLng = new LatLng(_sign.getLatitude(),_sign.getLongitude());

        try {
            MapsInitializer.initialize(this.getActivity());
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }


        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 10);
        map.animateCamera(cameraUpdate);

        map.addMarker(new MarkerOptions()
                .title(_sign.getSignTitle())
                .snippet(_sign.getSignDescription())
                .position(latLng));
    }


}
