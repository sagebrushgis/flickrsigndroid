package com.sagebrushgis.highwaysignviewer.ui.fragments;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Point;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.sagebrushgis.highwaysignviewer.HighwaySignViewerApplication;
import com.sagebrushgis.highwaysignviewer.R;
import com.sagebrushgis.highwaysignviewer.Sign;
import com.sagebrushgis.highwaysignviewer.da.Highway;
import com.sagebrushgis.highwaysignviewer.events.DetailsVisibleEvent;
import com.sagebrushgis.highwaysignviewer.ui.MapActivity;
import com.squareup.otto.Bus;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import javax.inject.Inject;

/**
 * Created with IntelliJ IDEA.
 * User: zachm
 * Date: 4/17/13
 * Time: 12:41 PM
 * To change this template use File | Settings | File Templates.
 */
public class DetailsFragment extends Fragment {


    TextView titleText;
    TextView descText;
    LinearLayout linearLayout;
    View horizLineMid;
    TextView placeText;

    ImageView mapImage;
    private Sign s;

    @Inject
    protected Bus bus;

    public static DetailsFragment newInstance(Sign sign) {
        DetailsFragment f = new DetailsFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putParcelable("sign", sign);
        f.setArguments(args);

        return f;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        s = getArguments().getParcelable("sign");
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_details,container, false);


        ((HighwaySignViewerApplication)getActivity().getApplication()).inject(this);

        titleText = (TextView)view.findViewById(R.id.titleText);
        descText = (TextView)view.findViewById(R.id.descText);
        linearLayout = (LinearLayout)view.findViewById(R.id.highwayTags);
        placeText = (TextView)view.findViewById(R.id.place);

        mapImage = (ImageView)view.findViewById(R.id.map);

        bus.post(new DetailsVisibleEvent(titleText));

        titleText.setText(s.getSignTitle());

        String desc = s.getSignDescription();

        if (desc != "null")
            descText.setText(desc);
        else
            descText.setVisibility(View.GONE);


        placeText.setText(s.getPlace() + ", " + s.getState());
        //Highways
        for (Highway h : s.getHighways()){
            View v = inflater.inflate(R.layout.shield_list, null);

            TextView newTag = (TextView)v.findViewById(R.id.highway);
            ImageView shield = (ImageView)v.findViewById(R.id.shield_image);
            shield.setScaleType(ImageView.ScaleType.CENTER);

            String url = getString(R.string.large_shield_url) + h.getImageName();
            Picasso.with(getActivity()).load(url).into(shield);

            newTag.setText(h.getHighway());
            linearLayout.addView(v);
        }

        String location = s.getLatitude() + "," + s.getLongitude();

        int orientation = getScreenOrientation();

        int mapWidth = 300;
        int mapHeight = 300;
        if (orientation == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE ||
                orientation == ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE){
            mapWidth = 500;
            mapHeight = 200;
        }

        StringBuilder mapUrl = new StringBuilder("http://maps.googleapis.com/maps/api/staticmap?");
        mapUrl.append("size=" + mapWidth + "x" + mapHeight + "&scale=2&center=");
        mapUrl.append(location);
        mapUrl.append("&zoom=12&sensor=false");
        mapUrl.append("&markers=color:red%7C");
        mapUrl.append(location);
        Picasso.with(getActivity()).load(mapUrl.toString()).into(mapImage);



        return view;
    }

    private int getScreenOrientation() {
        int rotation = getActivity().getWindowManager().getDefaultDisplay().getRotation();
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        int orientation;
        // if the device's natural orientation is portrait:
        if ((rotation == Surface.ROTATION_0
                || rotation == Surface.ROTATION_180) && height > width ||
                (rotation == Surface.ROTATION_90
                        || rotation == Surface.ROTATION_270) && width > height) {
            switch(rotation) {
                case Surface.ROTATION_0:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                    break;
                case Surface.ROTATION_90:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
                    break;
                case Surface.ROTATION_180:
                    orientation =
                            ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
                    break;
                case Surface.ROTATION_270:
                    orientation =
                            ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
                    break;
                default:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                    break;
            }
        }
        // if the device's natural orientation is landscape or if the device
        // is square:
        else {
            switch(rotation) {
                case Surface.ROTATION_0:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
                    break;
                case Surface.ROTATION_90:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                    break;
                case Surface.ROTATION_180:
                    orientation =
                            ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
                    break;
                case Surface.ROTATION_270:
                    orientation =
                            ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
                    break;
                default:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
                    break;
            }
        }

        return orientation;
    }

}
