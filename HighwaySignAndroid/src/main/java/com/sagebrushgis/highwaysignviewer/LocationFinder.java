package com.sagebrushgis.highwaysignviewer;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.util.Log;

/**
 * Created by zachm on 1/29/14.
 */
public class LocationFinder  {
    private double _longitude;
    private double _latitude;
    private LocationStatus _status;

    private String _provider;
    private LocationManager _locationManager;

    public LocationFinder(LocationStatus locationStatus, LocationManager locationManager){
        _status = locationStatus;
        _locationManager = locationManager;
    }

    public void acquireLocation(){
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);

        //Try network forst
        LocationProvider provider = _locationManager.getProvider("network");
        if (provider != null)
            _provider = provider.getName();
        else
            _provider = _locationManager.getBestProvider(criteria, true);


        _locationManager.requestLocationUpdates(_provider, 400, 10000, listener);
    }

    public void stopListening(){
        _locationManager.removeUpdates(listener);
    }

    public interface LocationStatus{
        void onLocationComplete(double latitude, double longitude);
    }

    private final LocationListener listener = new LocationListener(){

        @Override
        public void onLocationChanged(Location location) {
            _longitude = location.getLongitude();
            _latitude = location.getLatitude();

            _status.onLocationComplete(_latitude,_longitude);
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {
            //To change body of implemented methods use File | Settings | File Templates.
        }

        @Override
        public void onProviderEnabled(String s) {
            //To change body of implemented methods use File | Settings | File Templates.
        }

        @Override
        public void onProviderDisabled(String s) {
            //To change body of implemented methods use File | Settings | File Templates.
        }
    };
}
