package com.sagebrushgis.highwaysignviewer.events;

import com.sagebrushgis.highwaysignviewer.da.Highway;
import com.sagebrushgis.highwaysignviewer.da.HighwayType;

/**
 * Created by zachm on 2/6/14.
 */
public class HighwayTypeHighwayEvent {
    public HighwayType _type;
    public Highway _highway;

    public HighwayTypeHighwayEvent(HighwayType type, Highway highway){
        _type = type;
        _highway = highway;
    }

    public HighwayType getType() {
        return _type;
    }

    public void setType(HighwayType type) {
        this._type = type;
    }

    public Highway getHighway() {
        return _highway;
    }

    public void setHighway(Highway highway) {
        this._highway = highway;
    }
}
