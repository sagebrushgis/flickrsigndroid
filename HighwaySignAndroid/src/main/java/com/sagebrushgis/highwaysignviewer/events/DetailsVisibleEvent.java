package com.sagebrushgis.highwaysignviewer.events;

import android.view.View;

/**
 * Created by zachm on 3/17/14.
 */
public class DetailsVisibleEvent {

    private View _view;
    public DetailsVisibleEvent(View view){
        _view = view;
    }

    public void setView(View view){
        _view  = view;
    }

    public View getView(){
        return _view;
    }
}
