package com.sagebrushgis.highwaysignviewer.events;

import com.sagebrushgis.highwaysignviewer.Coordinates;
import com.sagebrushgis.highwaysignviewer.Sign;

import java.util.List;

/**
 * Created by zachm on 2/8/14.
 */
public class GeoQueryCompletedEvent {

    private Coordinates _coordinates;
    private List<Sign> _signs;
    private int _page;
    private int _pages;

    public GeoQueryCompletedEvent(Coordinates coordinates, List<Sign> signs, int page, int pages) {
        this._coordinates = coordinates;
        this._signs = signs;
        this._page = page;
        this._pages = pages;
    }

    public Coordinates getCoordinates() {
        return _coordinates;
    }

    public void setCoordinates(Coordinates coordinates) {
        this._coordinates = coordinates;
    }

    public List<Sign> getSigns() {
        return _signs;
    }

    public void setSigns(List<Sign> signs) {
        this._signs = signs;
    }

    public int getPage() {
        return _page;
    }

    public void setPage(int page) {
        this._page = page;
    }

    public int getPages() {
        return _pages;
    }

    public void setPages(int pages) {
        this._pages = pages;
    }


}
