package com.sagebrushgis.highwaysignviewer.events;

/**
 * Created by zachm on 2/18/14.
 */
public class AddFavoritesEvent {
    private long _imageId;

    public AddFavoritesEvent(long imageId){
        _imageId = imageId;
    }

    public void setImageId(long imageId){
        _imageId = imageId;
    }

    public long getImageId(){
        return _imageId;
    }
}
