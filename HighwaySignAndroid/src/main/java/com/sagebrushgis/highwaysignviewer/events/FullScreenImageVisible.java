package com.sagebrushgis.highwaysignviewer.events;

import android.view.View;

import com.sagebrushgis.highwaysignviewer.Sign;

/**
 * Created by zachm on 3/17/14.
 */
public class FullScreenImageVisible {
    private Sign _sign;

    public FullScreenImageVisible(Sign sign){
        _sign = sign;
    }

    public void setSign(Sign sign){
        _sign = sign;
    }

    public Sign getSign(){
        return _sign;
    }

}
