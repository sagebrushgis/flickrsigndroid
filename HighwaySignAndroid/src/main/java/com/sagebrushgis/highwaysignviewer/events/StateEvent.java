package com.sagebrushgis.highwaysignviewer.events;

import com.sagebrushgis.highwaysignviewer.MainActivity;
import com.sagebrushgis.highwaysignviewer.da.Country;
import com.sagebrushgis.highwaysignviewer.da.State;

/**
 * Created by zachm on 2/13/14.
 */
public class StateEvent {


    private State _state;
    private MainActivity.BrowseType _browseType;

    public StateEvent(State state, MainActivity.BrowseType browseType) {
        this._state = state;
        this._browseType = browseType;
    }

    public State getState() {
        return _state;
    }

    public void setCountry(State state) {
        this._state = state;
    }

    public MainActivity.BrowseType getBrowseType() {
        return _browseType;
    }

    public void setBrowseType(MainActivity.BrowseType browseType) {
        this._browseType = browseType;
    }
}

