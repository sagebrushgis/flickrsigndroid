package com.sagebrushgis.highwaysignviewer.events;

import com.sagebrushgis.highwaysignviewer.da.Highway;
import com.sagebrushgis.highwaysignviewer.da.State;

/**
 * Created by zachm on 1/28/14.
 */
public class HighwayStateEvent {
    private Highway _highway;
    private State _state;

    public HighwayStateEvent(){

    }

    public HighwayStateEvent(Highway highway, State state) {
        this._highway = highway;
        this._state = state;
    }

    public Highway getHighway() {
        return _highway;
    }

    public void setHighway(Highway highway) {
        this._highway = highway;
    }

    public State getState() {
        return _state;
    }

    public void setState(State state) {
        this._state = state;
    }
}
