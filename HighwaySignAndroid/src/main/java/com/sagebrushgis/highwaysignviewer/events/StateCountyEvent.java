package com.sagebrushgis.highwaysignviewer.events;

import com.sagebrushgis.highwaysignviewer.da.County;
import com.sagebrushgis.highwaysignviewer.da.State;

/**
 * Created by zachm on 2/16/14.
 */
public class StateCountyEvent {
    private County _county;
    private State _state;

    public StateCountyEvent(County county, State state) {
        this._county = county;
        this._state = state;
    }

    public County getCounty() {
        return _county;
    }

    public void setCounty(County county) {
        this._county = county;
    }

    public State getState() {
        return _state;
    }

    public void setState(State state) {
        this._state = state;
    }
}
