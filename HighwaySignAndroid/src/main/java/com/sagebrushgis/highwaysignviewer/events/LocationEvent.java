package com.sagebrushgis.highwaysignviewer.events;

/**
 * Created by zachm on 2/1/14.
 */
public class LocationEvent {
    private double _latitude;
    private double _longitude;

    public LocationEvent(double latitude, double longitude){
        _latitude = latitude;
        _longitude = longitude;
    }

    public double getLatitude(){
        return _latitude;
    }
    public void setLatitude(double latitude){
        this._latitude = latitude;
    }

    public double getLongitude(){
        return _longitude;
    }

    public void setLongitude(double longitude){
        this._longitude = longitude;
    }
}
