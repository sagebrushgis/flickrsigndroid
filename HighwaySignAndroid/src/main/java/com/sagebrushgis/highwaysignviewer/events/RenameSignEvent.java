package com.sagebrushgis.highwaysignviewer.events;

import com.sagebrushgis.highwaysignviewer.Sign;

/**
 * Created by zachm on 2/8/14.
 */
public class RenameSignEvent {
    private Sign _sign;
    private boolean _initial;

    public RenameSignEvent(Sign sign, boolean initial){
        this._sign = sign;
        this._initial = initial;
    }

    public Sign getSign() {
        return _sign;
    }

    public void setSign(Sign sign) {
        this._sign = sign;
    }

    public boolean isInitial() {
        return _initial;
    }

    public void setInitial(boolean initial) {
        this._initial = initial;
    }




}
