package com.sagebrushgis.highwaysignviewer;

import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.IBinder;
import android.widget.RemoteViews;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by zachm on 4/29/14.
 */
public class UpdateRandomWidgetService extends Service{

    @Inject
    protected OkHttpClient _httpClient;

    @Inject
    protected Picasso picasso;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        //Set up this class for DI
        ((HighwaySignViewerApplication)getApplication()).inject(this);


        final AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this.getApplicationContext());

        int[] allWidgetIds = intent.getIntArrayExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS);

        ComponentName thisWidget = new ComponentName(getApplicationContext(),RandomSignWidgetProvider.class);

        final Context ctx = this.getApplicationContext();
        for (final int widgetId : allWidgetIds){

            final RemoteViews remoteViews = new RemoteViews(this.getApplicationContext().getPackageName(),R.layout.widget_layout);

            // Register an onClickListener
            Intent clickIntent = new Intent(this.getApplicationContext(),
                    RandomSignWidgetProvider.class);

            clickIntent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
            clickIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS,
                    allWidgetIds);

            PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(),0,
                    clickIntent,PendingIntent.FLAG_UPDATE_CURRENT);

            remoteViews.setOnClickPendingIntent(R.id.randomImage, pendingIntent);

            ImageQueries queries = new ImageQueries(new ImageQueries.ImageQueryResult() {
                @Override
                public void OnImageQueryComplete(ImageQueries.QueryType queryType, List<Sign> signs, int page, int pages, boolean initial) {

                    if (signs.size() >= 1) {
                        new DownloadImage(remoteViews,widgetId,appWidgetManager).execute(signs.get(0).getLargeImageURL());
                    }
                }
            },_httpClient);
            queries.getRandomImage(false);

        }

        return Service.START_NOT_STICKY;
    }

    private class DownloadImage extends AsyncTask<String,Void,Bitmap>{

        private RemoteViews _views;
        private int _appWidgetId;
        private AppWidgetManager _manager;

        public DownloadImage(RemoteViews views, int appWidgetId, AppWidgetManager manager){
            _views = views;
            _appWidgetId = appWidgetId;
            _manager = manager;
        }

        @Override
        protected Bitmap doInBackground(String... params)  {
            String url = params[0];
            Bitmap bitmap = null;

            InputStream inStream = null;

            try {
                HttpURLConnection conn = _httpClient.open(new URL(url));

                inStream = conn.getInputStream();
                bitmap = BitmapFactory.decodeStream(inStream);

            }
            catch(Exception ex){

            }

            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (isCancelled()){
                bitmap = null;
            }

            _views.setImageViewBitmap(R.id.randomImage, bitmap);
            _manager.updateAppWidget(_appWidgetId, _views);
        }
    }


}
