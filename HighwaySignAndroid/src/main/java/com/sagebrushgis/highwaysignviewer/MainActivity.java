package com.sagebrushgis.highwaysignviewer;



import java.util.ArrayList;
import java.util.List;


import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;
import android.widget.*;



import android.content.Context;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;

import javax.inject.Inject;


import com.sagebrushgis.highwaysignviewer.da.County;
import com.sagebrushgis.highwaysignviewer.da.DrawerItem;
import com.sagebrushgis.highwaysignviewer.da.HighwayDataSource;
import com.sagebrushgis.highwaysignviewer.da.HighwayType;
import com.sagebrushgis.highwaysignviewer.da.Item;
import com.sagebrushgis.highwaysignviewer.events.AddFavoritesEvent;
import com.sagebrushgis.highwaysignviewer.events.DetailsVisibleEvent;
import com.sagebrushgis.highwaysignviewer.events.FullScreenImageVisible;
import com.sagebrushgis.highwaysignviewer.events.HighwayStateEvent;
import com.sagebrushgis.highwaysignviewer.events.HighwayTypeHighwayEvent;
import com.sagebrushgis.highwaysignviewer.events.LocationEvent;
import com.sagebrushgis.highwaysignviewer.events.NoSignsEvent;
import com.sagebrushgis.highwaysignviewer.events.RenameSignEvent;
import com.sagebrushgis.highwaysignviewer.events.StateCountyEvent;
import com.sagebrushgis.highwaysignviewer.events.StateEvent;
import com.sagebrushgis.highwaysignviewer.ui.fragments.CountyFragment;
import com.sagebrushgis.highwaysignviewer.ui.fragments.DetailsFragment;
import com.sagebrushgis.highwaysignviewer.ui.fragments.FavoritesDialog;
import com.sagebrushgis.highwaysignviewer.ui.fragments.FullScreenImageFragment;
import com.sagebrushgis.highwaysignviewer.ui.fragments.HighwayFragment;
import com.sagebrushgis.highwaysignviewer.ui.fragments.HighwayTypeFragment;
import com.sagebrushgis.highwaysignviewer.ui.fragments.NoNetworkConnectionDialog;
import com.sagebrushgis.highwaysignviewer.ui.fragments.NoSignsDialogFragment;
import com.sagebrushgis.highwaysignviewer.ui.fragments.ResultsFragment;
import com.sagebrushgis.highwaysignviewer.ui.fragments.SearchFragment;
import com.sagebrushgis.highwaysignviewer.ui.fragments.StateFragment;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

public class MainActivity extends ActionBarActivity
        implements ImageQueries.ImageQueryResult, LocationFinder.LocationStatus,
        NoSignsDialogFragment.DialogFragmentListener {

    public enum BrowseType{
        BrowseStateHighway,
        BrowseHighwayType,
        BrowseStateCounty
    }


    @Inject
    protected Bus bus;

    @Inject
    protected OkHttpClient _client;

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle _drawerToggle;
    private SlidingUpPanelLayout _slidingPanel;

    private LocationFinder _finder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((HighwaySignViewerApplication)getApplication()).inject(this);
        bus.register(this);

        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_main);
        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        mDrawerList = (ListView)findViewById(R.id.left_drawer);
        _slidingPanel = (SlidingUpPanelLayout)findViewById(R.id.uppanel);


        //Start off hidden
        _slidingPanel.hidePane();

        float panelHeight = dipToPixels(this,60.0f);
        _slidingPanel.setPanelHeight((int)Math.floor(panelHeight));
        _slidingPanel.setEnableDragViewTouchEvents(false);
        _slidingPanel.setPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {


            }

            @Override
            public void onPanelCollapsed(View panel) {
                ImageView iv = (ImageView)panel.findViewById(R.id.collapseExpand);
                iv.setImageResource(R.drawable.ic_action_collapse);
            }

            @Override
            public void onPanelExpanded(View panel) {
                ImageView iv = (ImageView)panel.findViewById(R.id.collapseExpand);
                iv.setImageResource(R.drawable.ic_action_expand);
            }

            @Override
            public void onPanelAnchored(View panel) {
                ImageView iv = (ImageView)panel.findViewById(R.id.collapseExpand);
                iv.setVisibility(View.VISIBLE);
            }
        });
        _finder = new LocationFinder(this,(LocationManager)getSystemService(Context.LOCATION_SERVICE));

        //Set the adapter for drawer
        mDrawerList.setAdapter(BuildDrawerAdapter());
        //mDrawerList.setAdapter(new ArrayAdapter<String>(this, R.layout.drawer_list_item, mdrawerItems));
        mDrawerList.setAdapter(BuildDrawerAdapter());
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());



        _drawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer,
                R.string.drawer_open, R.string.drawer_close){
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getSupportActionBar().setTitle("Highway Sign Finder");
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle("Highway Sign Finder");
                invalidateOptionsMenu();
            }
        };

        mDrawerLayout.setDrawerListener(_drawerToggle);


        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME
                | ActionBar.DISPLAY_SHOW_TITLE | ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        if (savedInstanceState == null){
            loadInitialRandomActivity();
        }
    }

    private HeaderArrayAdapter BuildDrawerAdapter(){
        List<Item> listItems = new ArrayList<Item>();

        //listItems.add(new DrawerItem(true,"Browse Signs By"));
        listItems.add(new DrawerItem(Item.ListHeaderType.Default,"Browse By State"));
        listItems.add(new DrawerItem(Item.ListHeaderType.Default,"Browse By County"));
        listItems.add(new DrawerItem(Item.ListHeaderType.Default,"Browse By Highway"));

        //listItems.add(new DrawerItem(true,"Show Signs"));
        listItems.add(new DrawerItem(Item.ListHeaderType.Default,"Signs At Current Location"));
        listItems.add(new DrawerItem(Item.ListHeaderType.Default,"Show Favorites"));

        return new HeaderArrayAdapter(this,listItems);
    }

    public float dipToPixels(Context context, float dipValue) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dipValue, metrics);
    }

    private void loadInitialRandomActivity(){
        if (isOnline()){
            ImageQueries query = new ImageQueries(this,_client);
            setSupportProgressBarIndeterminateVisibility(true);
            query.getRandomImage(true);
        }  else{
            // Insert the fragment by replacing any existing fragment
            FragmentManager fm = getSupportFragmentManager();
            NoNetworkConnectionDialog alertDialog = NoNetworkConnectionDialog.newInstance();

            alertDialog.show(fm, "fragment_alert");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_main, menu);

        return  true;
    }

    @Override
    public void onBackPressed() {
        resetPullUpMenu();
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (item.getItemId() == android.R.id.home) {

            if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
                mDrawerLayout.closeDrawer(mDrawerList);
            } else {
                mDrawerLayout.openDrawer(mDrawerList);
            }
            return true;
        }
        // Handle your other action bar items...

        switch (item.getItemId()){
            case R.id.action_refresh:
                resetPullUpMenu();
                // Create a new fragment and specify the planet to show based on position
                Fragment fragment = SearchFragment.newInstance();
                String backStateName = ((Object)fragment).getClass().getName();

                // Insert the fragment by replacing any existing fragment
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.content_frame, fragment)
                        .addToBackStack(backStateName)
                        .commit();
                break;
            case R.id.action_random:
                setSupportProgressBarIndeterminateVisibility(true);
                ImageQueries query = new ImageQueries(this,_client);
                query.getRandomImage(false);
            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        _drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        _drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void OnImageQueryComplete(ImageQueries.QueryType queryType,List<Sign> signs, int page, int pages, boolean initial){
        setSupportProgressBarIndeterminateVisibility(false);
        if (signs.size() > 0){
            bus.post(new RenameSignEvent(signs.get(0),initial));
        }
    }

    /**
     * Hide slider for all views but sign details
     */
    private void resetPullUpMenu(){
        _slidingPanel.hidePane();
    }

    private void selectItem(int position,Item item){
        resetPullUpMenu();
        if (item.getText() == "Browse By State"){
            // Create a new fragment and specify the planet to show based on position
            Fragment fragment = StateFragment.newInstance(BrowseType.BrowseStateHighway);
            String backStateName = ((Object)fragment).getClass().getName();
            // Insert the fragment by replacing any existing fragment
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, fragment)
                    .addToBackStack(backStateName)
                    .commit();

            // Highlight the selected item, update the title, and close the drawer
            mDrawerList.setItemChecked(position, true);
            //setTitle(mPlanetTitles[position]);
            mDrawerLayout.closeDrawer(mDrawerList);
        }else if (item.getText() == "Browse By Highway"){
            // Load the highway type fragment
            Fragment fragment = HighwayTypeFragment.newInstance();
            String backStateName = ((Object)fragment).getClass().getName();

            // Insert the fragment by replacing any existing fragment
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, fragment)
                    .addToBackStack(backStateName)
                    .commitAllowingStateLoss();

            // Highlight the selected item, update the title, and close the drawer
            mDrawerList.setItemChecked(position, true);
            mDrawerLayout.closeDrawer(mDrawerList);
        }else if (item.getText() == "Browse By County"){
            // Load the Browse State/County Fragment
            Fragment fragment = StateFragment.newInstance(BrowseType.BrowseStateCounty);
            String backStateName = ((Object)fragment).getClass().getName();

            // Insert the fragment by replacing any existing fragment
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, fragment)
                    .addToBackStack(backStateName)
                    .commitAllowingStateLoss();

            // Highlight the selected item, update the title, and close the drawer
            mDrawerList.setItemChecked(position, true);
            mDrawerLayout.closeDrawer(mDrawerList);
        }

        else if (item.getText() == "Signs At Current Location"){
            //Current location
            _finder.acquireLocation();

            // Highlight the selected item, update the title, and close the drawer
            mDrawerList.setItemChecked(position, true);
            //setTitle(mPlanetTitles[position]);
            mDrawerLayout.closeDrawer(mDrawerList);
        }else if (item.getText() == "Show Favorites"){

            HighwayDataSource ds = new HighwayDataSource(this);
            ds.open();

            // Create a new fragment and show results
            Fragment fragment = ResultsFragment.newInstance(ds.getAllFavorites());
            String backStateName = ((Object)fragment).getClass().getName();
            // Insert the fragment by replacing any existing fragment
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, fragment)
                    .addToBackStack(backStateName)
                    .commit();


            // Highlight the selected item, update the title, and close the drawer
            mDrawerList.setItemChecked(position, true);
            mDrawerLayout.closeDrawer(mDrawerList);
        }
    }

    @Override
    public void onLocationComplete(double latitude, double longitude) {
        _finder.stopListening();

        resetPullUpMenu();

        //Launch Results fragment
        Fragment fragment = ResultsFragment.newInstance(latitude,longitude,10);

        String backStateName = ((Object)fragment).getClass().getName();
        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, fragment)
                .addToBackStack(backStateName)
                .commit();

    }



    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView parent, View view, int position, long id) {
            Item item = (Item)mDrawerList.getItemAtPosition (position);
            selectItem(position,item);
        }


    }

    @Subscribe public void detailsVisible(DetailsVisibleEvent e){
        _slidingPanel.setDragView(e.getView());
    }

    @Subscribe public void fullScreenImageVisible(FullScreenImageVisible e){
        _slidingPanel.showPane();

        Fragment f = DetailsFragment.newInstance(e.getSign());
        FragmentManager fm = getSupportFragmentManager();

        fm.beginTransaction()
                .replace(R.id.slidepanel, f)
                .commit();
    }

    @Subscribe public void noSignsFound(NoSignsEvent e){
        FragmentManager fm = getSupportFragmentManager();
        NoSignsDialogFragment alertDialog = NoSignsDialogFragment.newInstance();

        alertDialog.show(fm, "fragment_alert");
    }

    @Subscribe public void locationChosen(LocationEvent locationEvent){
        resetPullUpMenu();
        //Launch Results fragment
        Fragment fragment = ResultsFragment.newInstance(locationEvent.getLatitude(),locationEvent.getLongitude(),10);
        String backStateName = ((Object)fragment).getClass().getName();
        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, fragment)
                .addToBackStack(backStateName)
                .commit();
    }


    @Subscribe public void highwayTypeChanged(HighwayType highwayType){
        resetPullUpMenu();

        //Launch highway fragment
        Fragment fragment = HighwayFragment.newInstance(highwayType);
        String backStateName = ((Object)fragment).getClass().getName();

        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, fragment)
                .addToBackStack(backStateName)
                .commit();
    }


    @Subscribe public void stateChanged(StateEvent state){
        resetPullUpMenu();
        if (state.getBrowseType() == BrowseType.BrowseStateHighway){
            //Launch highway fragment
            Fragment fragment = HighwayFragment.newInstance(state.getState());
            String backStateName = ((Object)fragment).getClass().getName();

            // Insert the fragment by replacing any existing fragment
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, fragment)
                    .addToBackStack(backStateName)
                    .commit();
        }else{
            //Launch highway fragment
            Fragment fragment = CountyFragment.newInstance(state.getState());
            String backStateName = ((Object)fragment).getClass().getName();

            // Insert the fragment by replacing any existing fragment
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, fragment)
                    .addToBackStack(backStateName)
                    .commit();
        }
    }

    @Subscribe public void countyChanged(StateCountyEvent stateCountyEvent){
        resetPullUpMenu();

        //Launch Results fragment
        Fragment fragment = ResultsFragment.newInstance(stateCountyEvent.getState(),stateCountyEvent.getCounty());
        String backStateName = ((Object)fragment).getClass().getName();

        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, fragment)
                .addToBackStack(backStateName)
                .commit();
    }

    @Subscribe public void highwayChanged(HighwayStateEvent highwayState){
        resetPullUpMenu();
        //Launch Results fragment
        Fragment fragment = ResultsFragment.newInstance(highwayState.getState(),highwayState.getHighway());
        String backStateName = ((Object)fragment).getClass().getName();

        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, fragment)
                .addToBackStack(backStateName)
                .commit();
    }

    @Subscribe public void highwayChanged(HighwayTypeHighwayEvent highwayState){
        resetPullUpMenu();
        //Launch Results fragment
        Fragment fragment = ResultsFragment.newInstance(highwayState.getHighway());
        String backStateName = ((Object)fragment).getClass().getName();

        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, fragment)
                .addToBackStack(backStateName)
                .commit();
    }

    @Subscribe public void resultChosen(RenameSignEvent sign){
        resetPullUpMenu();
        _slidingPanel.showPane();

        //Launch Sign details fragment
        Fragment fragment = FullScreenImageFragment.newInstance(sign.getSign());
        String backStateName = ((Object)fragment).getClass().getName();

        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getSupportFragmentManager();


        if (sign.isInitial()){
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, fragment)
                .commit();
        }
        else{
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, fragment)
                    .addToBackStack(backStateName)
                    .commit();
        }
    }


    @Subscribe public void OnAddFavoriteEvent(AddFavoritesEvent e){
        FragmentManager fm = getSupportFragmentManager();
        FavoritesDialog alertDialog = FavoritesDialog.newInstance(e.getImageId());

        alertDialog.show(fm, "fragment_favorites");
    }

    @Override
    public void onNoSignDialogFragmentClosing() {
        //Pop last item of back stack
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.popBackStack();

        ImageQueries query = new ImageQueries(this,_client);
        setSupportProgressBarIndeterminateVisibility(true);
        query.getRandomImage(false);
    }


    @Override
    protected void onPause() {
        super.onPause();
        bus.unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        bus.register(this);
    }

    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null &&
                cm.getActiveNetworkInfo().isConnectedOrConnecting();
    }

}