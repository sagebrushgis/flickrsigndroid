package com.sagebrushgis.highwaysignviewer;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
/**
 * Created by zachm on 1/8/14.
 */
public class SignAdapter extends BaseAdapter {

    private enum Direction{
        NORTH,
        EAST,
        SOUTH,
        WEST,
        NORTHWEST,
        NORTHEAST,
        SOUTHWEST,
        SOUTHEAST
    }

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
    private List<Sign> signs = new ArrayList<Sign>();
    private final Context ctx;
    private final Coordinates coordinates;

    public SignAdapter(Context ctx){
        this.ctx = ctx;
        this.coordinates = null;
    }

    public SignAdapter(Context ctx, Coordinates coordinates){
        this.ctx = ctx;
        this.coordinates = coordinates;
    }


    public void updateSigns(List<Sign> signs){
        if (this.signs.size() == 0)
            this.signs = signs;
        else
            this.signs.addAll(signs);
        notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return signs.size();
    }


    @Override
    public Sign getItem(int position) {
        return signs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return ((Object)signs.get(position)).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View result = convertView;

        if (result == null){
            LayoutInflater inflater = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            result = inflater.inflate(R.layout.list_row, parent, false);
        }


        final ProgressBar spinner = (ProgressBar)result.findViewById(R.id.idProgressBar);
        spinner.setVisibility(View.GONE); //Probably will remove this

        TextView place = (TextView)result.findViewById(R.id.place);
        TextView title = (TextView)result.findViewById(R.id.title);
        TextView state = (TextView)result.findViewById(R.id.state);
        TextView dateTaken = (TextView)result.findViewById(R.id.datetaken);
        TextView distance = (TextView)result.findViewById(R.id.distance);
        ImageView thumbImage = (ImageView)result.findViewById(R.id.list_image);

        Sign sign = signs.get(position);

        //Set values in list view
        title.setText(sign.getSignTitle());
        place.setText(sign.getPlace());
        state.setText(sign.getState());
        dateTaken.setText(dateFormat.format(sign.getDateTaken()));

        //Calculate distance for GEO types
        if (coordinates != null){
            Coordinates signCoord = new Coordinates(sign.getLatitude(),sign.getLongitude());
            double dist = distanceBetweenLocationsInMiles(coordinates, signCoord);

            String dir = textFromDirection(directionBetweenTwoPoints(coordinates,signCoord));
            distance.setText(String.format("%.2g", dist) + " Miles " + dir);

        }else{
            distance.setVisibility(View.GONE);
        }

        Picasso.with(ctx).load(sign.getSmallImageURL()).into(thumbImage);

        /*
        imageLoader.displayImage(sign.getSmallImageURL(), thumbImage, new SimpleImageLoadingListener(){
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                spinner.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                spinner.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                spinner.setVisibility(View.GONE);
            }
        });*/

        return result;
    }


    /**
     * Haversine Algorithm for calculating two points on great sphere
     * @return
     */
    private double distanceBetweenLocationsInMiles (Coordinates p1, Coordinates p2) {
        double R = 6371; // earth's mean radius in km
        double dLat  = Math.toRadians(p2.getLatitude() - p1.getLatitude());
        double dLong = Math.toRadians(p2.getLongitude() - p1.getLongitude());

        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(p1.getLatitude())) * Math.cos(Math.toRadians(p2.getLatitude()))
                        * Math.sin(dLong/2) * Math.sin(dLong/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double distKm =  R * c;

        return distKm * 0.621371; //Convert To Miles
    }

    private String textFromDirection(Direction direction){
        switch (direction){
            case NORTH:
                return "N";
            case EAST:
                return "E";
            case SOUTH:
                return "S";
            case WEST:
                return "W";
            case NORTHWEST:
                return "NW";
            case NORTHEAST:
                return "NE";
            case SOUTHWEST:
                return "SW";
            case SOUTHEAST:
                return "SE";
        }
        return "";
    }

    private Direction directionBetweenTwoPoints(Coordinates p1, Coordinates p2){
        double dLon = Math.toRadians(p2.getLongitude() - p1.getLongitude());
        double y = Math.sin(dLon) * Math.cos(p2.getLatitude());
        double x = Math.cos(p1.getLatitude())*Math.sin(p2.getLatitude()) -
                Math.sin(p1.getLatitude())*Math.cos(p2.getLatitude())*Math.cos(dLon);
        double bearing =  Math.toDegrees(Math.atan2(y, x));

        double degrees = (bearing + 360) % 360;

        if (degrees > 0 && degrees < 5)
            return Direction.NORTH;
        else if (degrees >=5 && degrees < 85)
            return Direction.NORTHEAST;
        else if (degrees >= 85 && degrees < 95)
            return Direction.EAST;
        else if (degrees >= 95 && degrees < 175)
            return  Direction.SOUTHEAST;
        else if (degrees >= 175 && degrees < 185)
            return Direction.SOUTH;
        else if (degrees >= 185 && degrees < 265)
            return Direction.SOUTHWEST;
        else if (degrees >= 265 && degrees < 275)
            return Direction.WEST;
        else if (degrees >= 275 && degrees < 355)
            return Direction.NORTHWEST;
        else
            return Direction.NORTH;
    }
}
