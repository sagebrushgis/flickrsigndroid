package com.sagebrushgis.highwaysignviewer;

/**
 * Created by zachm on 3/11/14.
 */

import android.app.Application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(
     includes = {FlickrSignModule.class}
)
public final class HighwaySignViewerApplicationModule {
    private HighwaySignViewerApplication _app;

    public HighwaySignViewerApplicationModule(HighwaySignViewerApplication app){
        _app = app;
    }

     @Provides
     @Singleton
     public Application provideApplication(){
         return _app;
     }
}
