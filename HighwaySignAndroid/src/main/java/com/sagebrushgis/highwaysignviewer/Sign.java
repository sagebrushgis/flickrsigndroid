package com.sagebrushgis.highwaysignviewer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import android.os.Parcel;
import android.os.Parcelable;

import com.sagebrushgis.highwaysignviewer.da.Highway;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Sign implements Parcelable{
    private long _flickrID;
    private String _signTitle;
    private String _signDescription;
    private Date _dateTaken;
    private String _thumbnailURL;
    private String _smallImageURL;
    private String _mediumImageURL;
    private String _largeImageURL;
    private ArrayList<String> _tags;
    private Double _latitude;
    private Double _longitude;
    private String _state;
    private String _country;
    private String _county;
    private String _place;
    private ArrayList<Highway> _highways;

    static SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

    public Sign(){

    }

    public static Sign buildFromJson(JSONObject json) throws ParseException,JSONException{
        Sign sign = new Sign();

        sign.setState(json.getString("state"));
        sign.setLargeImageURL(json.getString("largeimage"));
        sign.setSmallImageURL(json.getString("smallimage"));
        sign.setThumbnailURL(json.getString("thumbnail"));
        sign.setMediumImageURL(json.getString("mediumimage"));
        sign.setLatitude(json.getDouble("latitude"));
        sign.setLongitude(json.getDouble("longitude"));
        sign.setSignTitle(json.getString("title"));
        sign.setSignDescription(json.getString("description"));
        sign.setSignTitle(json.getString("title"));
        sign.setDateTaken(dateFormat.parse(json.getString("date")));
        sign.setState(json.getString("state"));
        sign.setCountry(json.getString("country"));
        sign.setCounty(json.getString("county"));
        sign.setPlace(json.getString("place"));
        sign.setFlickrID(json.getLong("id"));

        ArrayList<Highway> highways = new ArrayList<Highway>();
        JSONArray hwyJsonArray = json.getJSONArray("highways");
        for (int j = 0; j <= hwyJsonArray.length() - 1; j++){
            Highway hwy = new Highway();
            JSONObject hwyJson = hwyJsonArray.getJSONObject(j);
            hwy.setHighway(hwyJson.getString("name"));
            hwy.setImageName(hwyJson.getString("image"));
            highways.add(hwy);
        }

        sign.setHighways(highways);

        return sign;
    }

    public Sign(Parcel source){
        _flickrID = source.readLong();
        _signTitle = source.readString();
        _signDescription = source.readString();
        _dateTaken = new Date(source.readLong());
        _thumbnailURL = source.readString();
        _smallImageURL = source.readString();
        _mediumImageURL = source.readString();
        _largeImageURL = source.readString();
        _tags = new ArrayList<String>();
        source.readStringList(_tags);
        _latitude = source.readDouble();
        _longitude = source.readDouble();
        _state = source.readString();
        _country = source.readString();
        _county = source.readString();
        _place = source.readString();
        Parcelable[] highwayTemp = source.readParcelableArray(Highway.class.getClassLoader());
        _highways = new ArrayList<Highway>();
        for (Parcelable o : highwayTemp){
            _highways.add((Highway)o);
        }

    }


    public String getState(){
        return this._state;
    }

    public void setState(String state){
        this._state = state;
    }

    public Double getLongitude(){
        return this._longitude;
    }

    public void setLongitude(Double longitude){
        this._longitude = longitude;
    }

    public Double getLatitude(){
        return this._latitude;
    }

    public void setLatitude(Double latitude){
        this._latitude = latitude;
    }

    public ArrayList<String> getTags(){
        return this._tags;
    }

    public void setTags(ArrayList<String> tags){
        this._tags = tags;
    }

    public long getFlickrID(){
        return this._flickrID;
    }

    public void setFlickrID(long flickrID){
        this._flickrID = flickrID;
    }


    public String getThumbnailURL(){
        return this._thumbnailURL;
    }

    public void setThumbnailURL(String thumbnailURL){
        this._thumbnailURL = thumbnailURL;
    }

    public void setSmallImageURL(String smallImageURL){
        this._smallImageURL = smallImageURL;
    }

    public String getSmallImageURL(){
        return this._smallImageURL;
    }

    public void setMediumImageURL(String mediumImageURL){
        this._mediumImageURL = mediumImageURL;
    }

    public String getMediumImageURL(){
        return this._mediumImageURL;
    }

    public void setLargeImageURL(String largeImageURL){
        this._largeImageURL = largeImageURL;
    }

    public String getLargeImageURL(){
        return this._largeImageURL;
    }

    public String getSignTitle() {
        return _signTitle;
    }
    public void setSignTitle(String signTitle) {
        this._signTitle = signTitle;
    }
    public String getSignDescription() {
        return _signDescription;
    }
    public void setSignDescription(String signDescription) {
        this._signDescription = signDescription;
    }


    public void setDateTaken(Date dateTaken){
        this._dateTaken = dateTaken;
    }

    public Date getDateTaken(){
        return this._dateTaken;
    }

    public void setCountry(String country){this._country = country;}

    public String getCountry(){return this._country;}

    public void setCounty(String county){this._county = county;}

    public String getCounty(){return this._county;}

    public void setPlace(String place){this._place = place;}

    public String getPlace(){return this._place;}

    public void setHighways(ArrayList<Highway> highway){this._highways = highway;}

    public ArrayList<Highway> getHighways(){return this._highways;}

    public int describeContents() {
        return ((Object)this).hashCode();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(_flickrID);
        dest.writeString(_signTitle);
        dest.writeString(_signDescription);
        dest.writeLong(_dateTaken.getTime());
        dest.writeString(_thumbnailURL);
        dest.writeString(_smallImageURL);
        dest.writeString(_mediumImageURL);
        dest.writeString(_largeImageURL);
        dest.writeStringList(_tags);
        dest.writeDouble(_latitude);
        dest.writeDouble(_longitude);
        dest.writeString(_state);
        dest.writeString(_country);
        dest.writeString(_county);
        dest.writeString(_place);

        Parcelable[] p = new Parcelable[_highways.size()];
        for (int i = 0; i <= _highways.size() - 1; i++)
            p[i] = (Parcelable)_highways.get(i);

        dest.writeParcelableArray(p,flags);
    }

    public static final Parcelable.Creator<Sign> CREATOR = new Parcelable.Creator<Sign>() {

        public Sign createFromParcel(Parcel source) {
            return new Sign(source);
        }

        public Sign[] newArray(int size) {
            // TODO Auto-generated method stub
            return new Sign[size];
        }

    };

}