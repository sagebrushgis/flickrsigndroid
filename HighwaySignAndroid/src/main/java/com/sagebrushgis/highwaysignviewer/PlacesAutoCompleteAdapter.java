//https://developers.google.com/academy/apis/maps/places/autocomplete-android
package com.sagebrushgis.highwaysignviewer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;

public class PlacesAutoCompleteAdapter extends ArrayAdapter<String> implements
        Filterable {

    private ArrayList<String> resultList;
    private String _api_key;

    public PlacesAutoCompleteAdapter(Context context, int textViewResourceId, String api_key) {
        super(context, textViewResourceId);
        try {
            ApplicationInfo ai =
                    context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
            _api_key = ai.metaData.getString("com.sagebrushgis.highwaysignviewer.GoogleWebKey");
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getCount() {
        return resultList.size();
    }

    @Override
    public String getItem(int position) {
        return resultList.get(position);
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter(){

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();

                if (constraint != null && constraint.length() >= 4){
                    //Retreive autocomplete results
                    resultList = autocomplete(constraint.toString());

                    filterResults.values = resultList;
                    filterResults.count = resultList.size();
                }

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0){
                    notifyDataSetChanged();
                }else{
                    notifyDataSetInvalidated();
                }

            }

        };
        return filter;
    }

    private ArrayList<String> autocomplete(String s){
        ArrayList<String> predictionsArray = new ArrayList<String>();

        try{
            String url ="https://maps.googleapis.com/maps/api/place/autocomplete/json?input="+
                    URLEncoder.encode(s, "utf-8") +
                    "&sensor=true&key=" +
                    _api_key;

            URL googlePlaces = new URL(url);


            URLConnection tc = googlePlaces.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(tc.getInputStream()));

            String line;
            StringBuffer sb = new StringBuffer();
            //take Google's legible JSON and turn it into one big string.
            while ((line = in.readLine()) != null) {
                sb.append(line);
            }

            //turn that string into a JSON object
            JSONObject predictions = new JSONObject(sb.toString());
            //now get the JSON array that's inside that object
            JSONArray ja = new JSONArray(predictions.getString("predictions"));

            for (int i = 0; i < ja.length(); i++) {
                JSONObject jo = (JSONObject) ja.get(i);
                //add each entry to our array
                predictionsArray.add(jo.getString("description"));
            }
        }
        catch (IOException e){
            e.printStackTrace();
        }
        catch (JSONException e){
            e.printStackTrace();
        }

        // TODO Auto-generated method stub
        return predictionsArray;
    }
}