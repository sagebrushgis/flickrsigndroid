package com.sagebrushgis.highwaysignviewer;

import android.os.AsyncTask;

import com.google.common.base.Joiner;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.sagebrushgis.highwaysignviewer.da.County;
import com.sagebrushgis.highwaysignviewer.da.Favorite;
import com.sagebrushgis.highwaysignviewer.da.State;
import com.sagebrushgis.highwaysignviewer.events.GeoQueryCompletedEvent;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.otto.Bus;

import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by zachm on 1/18/14.
 */
public class ImageQueries  {


    public enum QueryType {GEO, STATE, RANDOM, HIGHWAY, COUNTY, FAVORITES}

    private ImageQueryResult _result;

    private OkHttpClient _client;
    public ImageQueries(ImageQueryResult result, OkHttpClient httpClient)
    {
        _client = httpClient;
        _result = result;
    }

    public void getRandomImage(boolean initial){
        new FlickrRequest().execute(QueryType.RANDOM, initial);
    }

    public void getImageByStateHighway(State state, String highway){

        new FlickrRequest().execute(QueryType.STATE, state.getName(), highway, 1);
    }

    public void getImageByHighway(String highway){

        new FlickrRequest().execute(QueryType.HIGHWAY, highway, 1);
    }


    public void getImageByLocation(Double latitude, Double longitude, int radius){
        new FlickrRequest().execute(QueryType.GEO,latitude, longitude, radius, 1);
    }

    public void getImageByStateCounty(State state, County county){
        new FlickrRequest().execute(QueryType.COUNTY,state.getName(), county.getName(), 1);
    }

    public void getImageByStateHighway(State state, String highway, int page){
        new FlickrRequest().execute(QueryType.STATE, state.getName(), highway, page);
    }

    public void getImageByHighway(String highway, int page){

        new FlickrRequest().execute(QueryType.HIGHWAY, highway, page);
    }

    public void getImageByLocation(Double latitude, Double longitude, int radius, int page){
        new FlickrRequest().execute(QueryType.GEO,latitude, longitude, radius, page);
    }

    public void getImageByStateCounty(State state, County county, int page){
        new FlickrRequest().execute(QueryType.COUNTY,state.getName(), county.getName(), page);
    }

    public void getFavoriteImages(List<Favorite> favorites){
        List<String> reqList = Lists.newArrayList();

        for (Favorite f : favorites){
            reqList.add(String.valueOf(f.getImageId()));
        }

        new FavoriteRequest().execute(Joiner.on('|').join(reqList));
    }


    private class FavoriteRequest extends AsyncTask<String,Void,String>{
        private final String _baseFavoriteUrl = "http://www.sagebrushgis.com/batchsign/";

        private String _favoriteList;

        @Override
        public String doInBackground(String...params) {
            //First parameter is the type enum
            _favoriteList = params[0];


            URL url = null;
            try {
                url = new URL(_baseFavoriteUrl);

                URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
                url = uri.toURL();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url.toString());
            httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
            // Request parameters and other properties.
            List<NameValuePair> nameValue = Lists.newArrayListWithCapacity(1);
            nameValue.add(new BasicNameValuePair("signlist", _favoriteList));


            try {
                httpPost.setEntity(new UrlEncodedFormEntity(nameValue, "UTF-8"));
                HttpEntity httpEntity = httpClient.execute(httpPost).getEntity();

                if (httpEntity != null) {
                    InputStream inputStream = httpEntity.getContent();
                    Reader in = new InputStreamReader(inputStream);
                    BufferedReader buffRead = new BufferedReader(in);
                    StringBuilder stringBuilder = new StringBuilder();

                    String curLine = null;

                    while ((curLine = buffRead.readLine()) != null) {
                        stringBuilder.append(curLine + "\n");
                    }


                    return stringBuilder.toString();
                }
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            List<Sign> signs = new ArrayList<Sign>();
            if (s.isEmpty())
                _result.OnImageQueryComplete(QueryType.FAVORITES,signs,1,1,false);


            SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

            JSONObject jsonObj = null;
            try {
                jsonObj = new JSONObject(s);
                JSONArray signArray = jsonObj.getJSONArray("signs");

                int page = 0;
                int pages = 0;
                if (jsonObj.has("page")){
                    page = jsonObj.getInt("page");
                    pages = jsonObj.getInt("pages");
                }
                for (int i = 0; i <= signArray.length() - 1; i++){
                    JSONObject json = signArray.getJSONObject(i);
                    Sign sign = Sign.buildFromJson(json);

                    signs.add(sign);
                }

                _result.OnImageQueryComplete(QueryType.FAVORITES, signs,page,pages,false);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    private class FlickrRequest extends AsyncTask<Object,Void,String> {
        private final String _baseRandomUrl = "http://www.sagebrushgis.com/random?format=json";
        private final String _baseQueryUrl = "http://www.sagebrushgis.com/query?";

        private boolean _initialState;
        private QueryType _queryType;


        @Override
        public String doInBackground(Object...params) {
            //First parameter is the type enum
            _queryType = (QueryType)params[0];

            String query = "";
            //Build query string
            if (_queryType == QueryType.RANDOM){
                query = _baseRandomUrl;
                _initialState = (Boolean)params[1];
            }
            else if (_queryType == QueryType.STATE)
                query = _baseQueryUrl + "type=state&state=" + params[1].toString() + "&highway=" + params[2].toString() + "&page=" + params[3].toString();
            else if (_queryType == QueryType.COUNTY)
                query = _baseQueryUrl + "type=county&state=" + params[1].toString() + "&county=" + params[2].toString() + "&page=" + params[3].toString();
            else if (_queryType == QueryType.GEO){
                query = _baseQueryUrl + "type=geo&lat=" + params[1].toString() + "&lon=" + params[2].toString() + "&radius=" + params[3].toString() + "&page=" + params[4].toString();
            }
            else if (_queryType == QueryType.HIGHWAY)
                query = _baseQueryUrl + "type=highway&highway=" + params[1].toString() + "&page=" + params[2].toString();

            URL url = null;
            try {
                url = new URL(query);

            URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
            url = uri.toURL();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            try {
                HttpURLConnection conn = _client.open(url);

                InputStream inputStream = conn.getInputStream();

                Reader in = new InputStreamReader(inputStream);
                BufferedReader buffRead = new BufferedReader(in);
                StringBuilder stringBuilder = new StringBuilder();

                String curLine = null;

                while ((curLine = buffRead.readLine()) != null) {
                    stringBuilder.append(curLine + "\n");
                }


                return stringBuilder.toString();
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            List<Sign> signs = new ArrayList<Sign>();
            if (s.isEmpty())
                _result.OnImageQueryComplete(_queryType,signs,1,1,_initialState);




            JSONObject jsonObj = null;
            try {
                jsonObj = new JSONObject(s);
                JSONArray signArray = jsonObj.getJSONArray("signs");

                int page = 0;
                int pages = 0;
                if (jsonObj.has("page")){
                    page = jsonObj.getInt("page");
                    pages = jsonObj.getInt("pages");
                }
                for (int i = 0; i <= signArray.length() - 1; i++){

                    JSONObject json = signArray.getJSONObject(i);
                    Sign sign = Sign.buildFromJson(json);

                    signs.add(sign);
                }



               _result.OnImageQueryComplete(_queryType, signs,page,pages,_initialState);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * Created by zachm on 1/18/14.
     */
    public interface ImageQueryResult {
        void OnImageQueryComplete(QueryType queryType, List<Sign> signs, int page, int pages, boolean initial);

    }
}

