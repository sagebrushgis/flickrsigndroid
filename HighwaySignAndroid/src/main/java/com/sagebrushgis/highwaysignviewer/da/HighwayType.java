package com.sagebrushgis.highwaysignviewer.da;


import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by zachm on 2/5/14.
 */
public class HighwayType implements Comparable<HighwayType>, Item, Parcelable{


    private int _sort;

    private String _hwyType;
    private String _slug;

    public HighwayType(){

    }

    public HighwayType(Parcel parcel){
        _sort = parcel.readInt();
        _hwyType = parcel.readString();
        _slug = parcel.readString();
    }

    public int getSort(){
        return this._sort;
    }

    public void setSort(int sort){
        _sort = sort;
    }

    public String getType(){
        return this._hwyType;
    }

    public void setType(String hwyType){
        this._hwyType = hwyType;
    }

    public String getSlug(){
        return this._slug;
    }

    public void setSlug(String slug){
        this._slug = slug;
    }


    @Override
    public String toString() {
        return this.getType();
    }

    @Override
    public int compareTo(HighwayType another) {
        int primary = this.getSort() - another.getSort();
        if (primary != 0){
            return primary;
        }

        return this.getType().compareTo(another.getType());
    }

    @Override
    public ListHeaderType getHeaderType() {
        return ListHeaderType.Default;
    }

    @Override
    public String getText() {
        return this.toString();
    }

    @Override
    public boolean showArrow() { return true; }

    @Override
    public int describeContents() {
        return ((Object)this).hashCode();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(_sort);
        dest.writeString(_hwyType);
        dest.writeString(_slug);
    }

    public static final Parcelable.Creator<HighwayType> CREATOR = new Parcelable.Creator<HighwayType>() {

        public HighwayType createFromParcel(Parcel source) {
            return new HighwayType(source);
        }

        public HighwayType[] newArray(int size) {
            return new HighwayType[size];
        }
    };
}
