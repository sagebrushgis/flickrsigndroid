package com.sagebrushgis.highwaysignviewer.da;


import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: zachm
 * Date: 4/12/13
 * Time: 8:18 PM
 * To change this template use File | Settings | File Templates.
 */
public class Country  implements Item {

    private String _name;
    private String _slug;

    private int _id;

    private List<State> _states;
    private List<HighwayType> _hwyTypes;

    public Country(){

    }


    public String getName() {
        return _name;
    }

    public void setName(String name) {
        this._name = name;
    }

    public String getSlug() {
        return _slug;
    }

    public void setSlug(String slug) {
        this._slug = slug;
    }

    public List<State> getStates(){return this._states;}

    public void setStates(List<State> states){this._states = states;}

    public List<HighwayType> getHighwayTypes(){return this._hwyTypes;}

    public void setHighwayTypes(List<HighwayType> hwyTypes){this._hwyTypes = hwyTypes;}

    @Override
    public String toString() {
        return _name;
    }

    @Override
    public ListHeaderType getHeaderType() {
        return ListHeaderType.SectionHeader;
    }

    @Override
    public String getText() {
        return this.toString();
    }

    @Override
    public boolean showArrow() { return false; }

}
