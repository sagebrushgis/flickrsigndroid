package com.sagebrushgis.highwaysignviewer.da;


import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created with IntelliJ IDEA.
 * User: zachm
 * Date: 4/12/13
 * Time: 8:18 PM
 * To change this template use File | Settings | File Templates.
 */
public class County implements Parcelable{

    private String _name;

    private String _type;

    private String _slug;

    public County(){}


    public County(Parcel parcel){
        _name = parcel.readString();
        _type = parcel.readString();
        _slug = parcel.readString();
    }

    public String getType() {
        return _type;
    }

    public void setType(String type) {
        this._type = type;
    }


    public String getName() {
        return _name;
    }

    public void setName(String name) {
        this._name = name;
    }


    public String getSlug() {
        return _slug;
    }

    public void setSlug(String slug) {
        this._slug = slug;
    }

    @Override
    public String toString() {
        return String.format("%s %s",_name,_type);
    }

    @Override
    public int describeContents() {
        return ((Object)this).hashCode();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(_name);
        dest.writeString(_type);
        dest.writeString(_slug);
    }

    public static final Parcelable.Creator<County> CREATOR = new Parcelable.Creator<County>() {

        public County createFromParcel(Parcel source) {
            return new County(source);
        }

        public County[] newArray(int size) {
            return new County[size];
        }

    };
}
