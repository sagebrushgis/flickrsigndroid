package com.sagebrushgis.highwaysignviewer.da;


import android.content.Context;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

public class HighwayDatabaseHelper extends SQLiteAssetHelper {


    private static String DB_NAME = "highway.db";
    private static final int VERSION = 7;

    public HighwayDatabaseHelper(Context context) {
        super(context, DB_NAME, null, VERSION);
        setForcedUpgrade();
    }

}