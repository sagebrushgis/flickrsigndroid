package com.sagebrushgis.highwaysignviewer.da;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by zachm on 2/19/14.
 */
public class Favorite implements Parcelable {
    public int _id;
    public long _imageId;

    public Favorite(){

    }


    public Favorite(Parcel parcel){
        _id = parcel.readInt();
        _imageId = parcel.readLong();
    }
    public int getId() {
        return _id;
    }

    public void setId(int id) {
        this._id = id;
    }

    public long getImageId() {
        return _imageId;
    }

    public void setImageId(long imageId) {
        this._imageId = imageId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(_id);
        dest.writeLong(_imageId);
    }

    public static final Parcelable.Creator<Favorite> CREATOR = new Parcelable.Creator<Favorite>() {

        public Favorite createFromParcel(Parcel source) {
            return new Favorite(source);
        }

        public Favorite[] newArray(int size) {
            return new Favorite[size];
        }

    };
}
