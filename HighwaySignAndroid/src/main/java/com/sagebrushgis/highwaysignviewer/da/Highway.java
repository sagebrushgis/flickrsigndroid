package com.sagebrushgis.highwaysignviewer.da;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

public class Highway  implements Comparable<Highway>, Parcelable{


    private String _highway;

    private String _tagID;

    private String _imageName;

    private int _sort;

    private HighwayType _type;

    public Highway(){

    }

    public Highway(Parcel source){
        _highway = source.readString();
        _tagID = source.readString();
        _imageName = source.readString();
        _sort = source.readInt();
        _type = source.readParcelable(HighwayType.class.getClassLoader());
    }


    public HighwayType getHighwayType(){
        return this._type;
    }

    public void setHighwayType(HighwayType highway){
        this._type = highway;
    }

    public String getHighway(){
        return this._highway;
    }

    public void setHighway(String highway){
        this._highway = highway;
    }

    public String getTagID(){
        return this._tagID;
    }

    public void setTagID(String tagID){
        this._tagID = tagID;
    }

    public String getImageName(){
        return this._imageName;
    }

    public void setImageName(String imageName){
        this._imageName = imageName;
    }


    @Override
    public String toString() {
        return this._highway;
    }

    @Override
    public int compareTo(Highway highway) {
        if (this._type == null || highway.getHighwayType() == null)
            return 0;

        return this._type.getSort() - highway._type.getSort();
    }

    public static Highway fromJson(JSONObject jsonObject) throws JSONException {
        Highway h = new Highway();
        h.setHighway(jsonObject.getString("name"));
        h.setTagID(jsonObject.getString("slug"));
        h.setImageName(jsonObject.getString("url"));

        HighwayType highwayType = new HighwayType();
        highwayType.setSort(jsonObject.getInt("sort"));
        highwayType.setType(jsonObject.getString("type"));
        highwayType.setSlug(jsonObject.getString("typeslug"));

        h.setHighwayType(highwayType);

        return h;
    }

    public int describeContents() {
        return ((Object)this).hashCode();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(_highway);
        dest.writeString(_tagID);
        dest.writeString(_imageName);
        dest.writeInt(_sort);
        dest.writeParcelable(_type,flags);
    }

    public static final Parcelable.Creator<Highway> CREATOR = new Parcelable.Creator<Highway>() {

        public Highway createFromParcel(Parcel source) {
            return new Highway(source);
        }

        public Highway[] newArray(int size) {
            // TODO Auto-generated method stub
            return new Highway[size];
        }

    };
}