package com.sagebrushgis.highwaysignviewer.da;


import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created with IntelliJ IDEA.
 * User: zachm
 * Date: 4/12/13
 * Time: 8:18 PM
 * To change this template use File | Settings | File Templates.
 */
public class State implements  Item, Parcelable {

    private int _id;

    private String _name;
    private String _slug;

    public State(){

    }

    public State(Parcel parcel){
        _id = parcel.readInt();
        _name = parcel.readString();
        _slug = parcel.readString();
    }


    public int getId(){return _id;}

    public void setId(int id){this._id = id;}



    public String getName() {
        return _name;
    }

    public void setName(String name) {
        this._name = name;
    }

    public String getSlug() {
        return _slug;
    }

    public void setSlug(String slug) {
        this._slug = slug;
    }

    @Override
    public String getText() {
        return getName();
    }

    @Override
    public String toString() {
        return this.toString();
    }

    @Override
    public boolean showArrow() { return true; }

    @Override
    public ListHeaderType getHeaderType() {
        return ListHeaderType.Default;
    }

    @Override
    public int describeContents() {
        return ((Object)this).hashCode();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(_id);
        dest.writeString(_name);
        dest.writeString(_slug);
    }

    public static final Parcelable.Creator<State> CREATOR = new Parcelable.Creator<State>() {

        public State createFromParcel(Parcel source) {
            return new State(source);
        }

        public State[] newArray(int size) {
            return new State[size];
        }

    };
}
