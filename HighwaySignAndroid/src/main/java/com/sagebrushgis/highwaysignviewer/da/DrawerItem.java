package com.sagebrushgis.highwaysignviewer.da;

/**
 * Created by zachm on 4/24/14.
 */
public class DrawerItem implements Item {

    private final ListHeaderType _headerType;
    private final String _listText;

    public DrawerItem(ListHeaderType headerType, String listText){
        _headerType = headerType;
        _listText = listText;
    }

    @Override
    public ListHeaderType getHeaderType() {
        return _headerType;
    }

    @Override
    public String getText() {
        return _listText;
    }

    @Override
    public boolean showArrow() { return false; }
}
