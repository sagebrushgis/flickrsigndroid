package com.sagebrushgis.highwaysignviewer.da;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

import javax.inject.Inject;


public class HighwayDataSource {

    private final HighwayDatabaseHelper _dbHelper;
    private SQLiteDatabase _database;


    public HighwayDataSource(Context context){
        _dbHelper = new HighwayDatabaseHelper(context);
    }

    public void open() throws SQLException{
        _database = _dbHelper.getWritableDatabase();
    }

    public void close(){
        _dbHelper.close();
    }

    public void clearFavorites(){
        _database.delete("favorites",null,null);
    }


   public void insertFavorite(long imageId){
       ContentValues values = new ContentValues();
       values.put("imageid", imageId);
       _database.insert("favorites", null, values);
   }


    public List<Favorite> getAllFavorites(){
        List<Favorite> countries = new ArrayList<Favorite>();

        Cursor cursor = _database.query("favorites",
                new String[]{"_id","imageid"}, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Favorite favorite = cursorToFavorite(cursor);
            countries.add(favorite);
            cursor.moveToNext();
        }
        // Make sure to close the cursor
        cursor.close();
        return countries;
    }




    private Favorite cursorToFavorite(Cursor cursor){
        Favorite f = new Favorite();
        f.setId(cursor.getInt(0));
        f.setImageId(cursor.getLong(1));

        return f;
    }

}