package com.sagebrushgis.highwaysignviewer.da;



/**
 * Created by zachm on 3/8/14.
 */
public interface Item {

    public enum ListHeaderType{
        Default,
        SectionHeader,
        SmallSettings
    }

    public ListHeaderType getHeaderType();
    public String getText();
    public boolean showArrow();
}
