package com.sagebrushgis.highwaysignviewer;


import android.app.Application;

import com.sagebrushgis.highwaysignviewer.ui.fragments.CountyFragment;
import com.sagebrushgis.highwaysignviewer.ui.fragments.DetailsFragment;
import com.sagebrushgis.highwaysignviewer.ui.fragments.FullScreenImageFragment;
import com.sagebrushgis.highwaysignviewer.ui.fragments.HighwayFragment;
import com.sagebrushgis.highwaysignviewer.ui.fragments.HighwayTypeFragment;
import com.sagebrushgis.highwaysignviewer.ui.fragments.ResultsFragment;
import com.sagebrushgis.highwaysignviewer.ui.fragments.SearchFragment;
import com.sagebrushgis.highwaysignviewer.ui.fragments.StateFragment;

import com.squareup.okhttp.HttpResponseCache;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(
        complete = false, //Needed so it will see other module
        injects = {HighwaySignViewerApplication.class,
        MainActivity.class,
        DetailsFragment.class,
        StateFragment.class,
        HighwayFragment.class,
        HighwayTypeFragment.class,
        ResultsFragment.class,
        SearchFragment.class,
        CountyFragment.class,
        FullScreenImageFragment.class,
        UpdateRandomWidgetService.class})
public class FlickrSignModule  {


    public FlickrSignModule(){

    }

    static final int DISK_CACHE_SIZE = 50 * 1024 * 1024; // 50MB

    @Provides @Singleton public Bus provideBus() {
        // our event bus running on any thread
        return new Bus(ThreadEnforcer.ANY);
    }

    @Provides @Singleton
     OkHttpClient provideOkHttpClient(Application app) {
        return createOkHttpClient(app);
    }

    @Provides @Singleton Picasso providePicasso(Application app, OkHttpClient client) {
        return new Picasso.Builder(app)
                .downloader(new OkHttpDownloader(client))
                .build();
    }

    static OkHttpClient createOkHttpClient(Application app) {
        OkHttpClient client = new OkHttpClient();

        // Install an HTTP cache in the application cache directory.
        try {
            File cacheDir = new File(app.getCacheDir(), "http");
            HttpResponseCache cache = new HttpResponseCache(cacheDir, DISK_CACHE_SIZE);
            client.setResponseCache(cache);
        } catch (IOException e) {
            //Log.e(e, "Unable to install disk cache.");
        }

        return client;
    }
}