package com.sagebrushgis.highwaysignviewer.tests;

import android.content.Intent;
import android.test.ActivityUnitTestCase;

import com.sagebrushgis.highwaysignviewer.MainActivity;

/**
 * Created by zachm on 3/2/14.
 */
public class MainActivityUnitTest extends ActivityUnitTestCase<MainActivity> {

    private MainActivity activity;

    public MainActivityUnitTest(){
        super(MainActivity.class);
    }


    @Override
    protected void setUp() throws Exception {
        super.setUp();
        Intent intent = new Intent(getInstrumentation().getTargetContext(),MainActivity.class);
        startActivity(intent,null,null);
        activity = getActivity();
    }


}
